package com.ract.ws.core.roadside;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import com.ract.util.DateTime;
import com.ract.util.Interval;

public class TestRoadside {
	RoadsideService service;
	DateTime startDate;
	DateTime endDate;
	Integer roadsideId;
	
	@Before
	public void setUp() throws Exception {
		
		endDate = new DateTime(); // now
		startDate = endDate.subtract(new Interval("0:1:0:0:0:0")); // 1 month ago		
		
		roadsideId = 94561;
		
		service = new RoadsideService();
	}

	@Test
	public void testRoadsideChangesCount() {
		Long count = service.getRoadsideChangesCount(startDate, endDate);
		assertTrue(count > 0);
	}
	
	@Test
	public void testRoadsideChangesIdentifiers() {
		Long count = service.getRoadsideChangesCount(startDate, endDate);
		assertTrue(count > 0);
		
		Collection<Integer> idList = service.getRoadsideChangesIdentifiers(startDate, endDate);
		assertTrue(idList != null && !idList.isEmpty());
		
		assertTrue(count == idList.size());
	}
	
	@Test
	public void testRoadsideChangesExist() {
		Long count = service.getRoadsideChangesCount(startDate, endDate);
		assertTrue(count > 0);
		
		assertTrue(service.isRoadsideChangesExist(startDate, endDate));
	}
	
	@Test
	public void testRoadsideChanges() {
		Long count = service.getRoadsideChangesCount(startDate, endDate);
		assertTrue(count > 0);
		
		Collection<Roadside> roadsideList = service.getRoadsideChanges(startDate, endDate);
		assertTrue(roadsideList != null && !roadsideList.isEmpty());

		assertTrue(count == roadsideList.size());
	}
	
	@Test
	public void testGetRoadsideById() {
		Roadside roadside = service.getRoadsideById(roadsideId);
		assertTrue(roadside != null && roadside.getRoadsideId().equals(roadsideId));
	}
}
