package com.ract.ws.core.client;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.ws.WebServiceException;

import org.junit.Before;
import org.junit.Test;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.SourceSystem;
import com.ract.common.ValidationException;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.ws.core.client.Client;
import com.ract.ws.core.client.ClientService;

public class TestClient {
	ClientService service;
	ClientMgr clientMgr;
	DateTime startDate;
	DateTime endDate;
	Integer clientNo;
	
	@Before
	public void setUp() throws Exception {

		endDate = new DateTime(); // now
		startDate = endDate.subtract(new Interval("0:0:1:0:0:0")); // 1 day ago
		clientNo = 432002;
		
		service = new ClientService();
		clientMgr = ClientEJBHelper.getClientMgr();
	}

	//@Test
	public void testClientChangesCount() {				
		Long count = service.getClientChangesCount(startDate, endDate);
		assertTrue(count > 0);
	}
	
	//@Test
	public void testClientChangesIdentifiers() {
		Long count = service.getClientChangesCount(startDate, endDate);
		assertTrue(count > 0);
		
		Collection<Integer> idList = service.getClientChangesIdentifiers(startDate, endDate);
		assertTrue(idList != null && !idList.isEmpty());
		
		assertTrue(count == idList.size());
	}
	
	//@Test
	public void testClientChangesExist() {
		assertTrue(service.isClientChangesExist(startDate, endDate));
	}
	
	//@Test
	public void testClientChanges() {
		Long count = service.getClientChangesCount(startDate, endDate);
		assertTrue(count > 0);
		
		Collection<Client> clientList = service.getClientChanges(startDate, endDate);
		assertTrue(clientList != null && !clientList.isEmpty());
		
		assertTrue(count == clientList.size());
	}
	
	//@Test
	public void testGetClientById() {
		Collection<Integer> idList = service.getClientChangesIdentifiers(startDate, endDate);
		assertTrue(idList != null && !idList.isEmpty());
		
		clientNo = (new ArrayList<Integer>(idList)).get(0);
		Client client = service.getClientById(clientNo);
		assertTrue(client != null && client.getClientNumber().equals(clientNo));
	}
	
	/**
	 * Test that MRM updates are excluded from ClientChanges.
	 * @throws ValidationException
	 * @throws RemoteException
	 */
	//@Test
	public void testExcludeUpdates() throws ValidationException, RemoteException {
		DateTime start = new DateTime();
		Client client = service.getClientById(clientNo);
		Integer returnedClientNo = service.updateClient(client, SourceSystem.MRM.getAbbreviation());		
		assertTrue(returnedClientNo.equals(clientNo));
		
		DateTime end = new DateTime();
		// MRM change should not count
		assertFalse(service.isClientChangesExist(start, end));
		
		start = new DateTime();
		returnedClientNo = service.updateClient(client, "WEB");
		assertTrue(returnedClientNo.equals(clientNo));
		
		end = new DateTime();
		// WEB change should count
		assertTrue(service.isClientChangesExist(start, end));		
	}
	
	@Test 
	public void testInsufficientClientData() {
		Client client = new Client();
		Integer newClientNo = null;
		client.setFirstName("Fail");
		client.setClientType(ClientVO.CLIENT_TYPE_PERSON);
		
		try {
			newClientNo = service.updateClient(client, SourceSystem.MRM.getAbbreviation());
		} catch (WebServiceException e) {
			assertTrue(e.getMessage().startsWith("Client minimum data requirements not met: "));			
		}
		assertNull(newClientNo);
	}
		
	//@Test
	public void testMRMUpdateClient() throws ValidationException, RemoteException {
		Client clientData = service.getClientById(clientNo);
		ClientVO clientVO = clientMgr.getClient(clientNo);
		DateTime before = clientVO.getLastUpdate();
		
		clientData.setResidentialStreetNumber("127");
		Integer returnedClientNo = service.updateClient(clientData, SourceSystem.MRM.getAbbreviation());
		clientVO = clientMgr.getClient(returnedClientNo);

		DateTime after = clientVO.getLastUpdate();
		
		assertTrue(after.after(before));
		assertTrue(clientVO.getResiStreetNumber().equals(127));
		assertTrue(clientVO.getSubSystem().equals(SourceSystem.MRM.getAbbreviation()));		
	}
	
	//@Test
	public void testMRMUpdateClientNewAddress() throws ValidationException, RemoteException {
		Client clientData = service.getClientById(clientNo);
		ClientVO clientVO = clientMgr.getClient(clientNo);
		
		Integer returnedClientNo = null;
		Integer oldResiStsubId = clientVO.getResiStsubId();
		
		clientData.setResidentialStreetName("IMAGINARY ST");
		
		returnedClientNo = service.updateClient(clientData, SourceSystem.MRM.getAbbreviation());
		clientVO = clientMgr.getClient(clientNo);
		
		assertNotNull(returnedClientNo);
		assertTrue(returnedClientNo.equals(clientNo)); // confirm we haven't created a new client
		assertTrue(!clientVO.getResiStsubId().equals(oldResiStsubId));
	}
	
	//@Test
	public void testMRMCreateClient() throws ValidationException, RemoteException {
		Client clientData = service.getClientById(clientNo);
		clientData.setClientNumber(null);
		
		Integer returnedClientNo = null;
				
		returnedClientNo = service.updateClient(clientData, SourceSystem.MRM.getAbbreviation());
		
		assertNotNull(returnedClientNo);
		System.out.println("New clientNo is: " + returnedClientNo);
		assertTrue(!returnedClientNo.equals(clientNo)); // should have different clientNo
	}
	
	//@Test
	public void testMRMCreateClientNewAddress() throws ValidationException, RemoteException {
		Client clientData = service.getClientById(clientNo);
		clientData.setClientNumber(null);
		
		Integer returnedClientNo = null;
		
		clientData.setResidentialStreetName("TEST ST");
		
		returnedClientNo = service.updateClient(clientData, SourceSystem.MRM.getAbbreviation());
		
		assertNotNull(returnedClientNo);
		System.out.println("New clientNo is: " + returnedClientNo);
		assertTrue(!returnedClientNo.equals(clientNo)); // should have different clientNo
	}
	
	@Test
	public void testStreetAbbreviation() {
		String streetName = "THE BOULEVARD";		
		String result = service.abbreviateStreetName(streetName);
		assertTrue(result.equals(streetName)); // unchanged
		
		streetName = "TEST ST";		
		result = service.abbreviateStreetName(streetName);
		assertTrue(result.equals(streetName)); // unchanged
		
		streetName = "ESPLANADE";		
		result = service.abbreviateStreetName(streetName);
		assertTrue(result.equals(streetName)); // unchanged
				
		streetName = "DODGY STREET,"; // strip dangling comma
		result = service.abbreviateStreetName(streetName);
		assertTrue(result.equals("DODGY ST"));
		
		streetName = "NANKOOR CRESCENT";		
		result = service.abbreviateStreetName(streetName);
		assertTrue(result.equals("NANKOOR CRES"));
		
		streetName = "NANKOOR BOULEVARD";		
		result = service.abbreviateStreetName(streetName);
		assertTrue(result.equals("NANKOOR BVD"));
	}
	
	@Test
	public void testRubbishEmails() {
		
		String testEmail = "To Follow";
		String result = Client.cleanseEmail(testEmail);
		assertTrue(result.isEmpty());
		
		testEmail = "g.newton@ract.com.au";
		result = Client.cleanseEmail(testEmail);
		assertTrue(result.equals(testEmail));
		
	}
}
