package com.ract.ws.receipting.client;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.common.ValidationException;
import com.ract.ws.receipting.ReceiptingResponse;

public class TestClient {

	ClientService service;
	Integer clientNo;
	
	@Before
	public void setUp() throws Exception {		
		service = new ClientService();
		clientNo = 432002;		
	}

	@Test
	public void testClient() throws ValidationException, RemoteException {
		ClientResponse response = service.getClient(clientNo);
		assertTrue(response.getStatus().equals(ReceiptingResponse.STATUS_SUCCESS));

		response = service.getClient(123456); // doesn't exist
		assertTrue(response.getStatus().equals(ReceiptingResponse.STATUS_ERROR));
	}
	
}
