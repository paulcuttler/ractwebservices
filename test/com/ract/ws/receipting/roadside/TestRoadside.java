package com.ract.ws.receipting.roadside;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.*;

import org.junit.Before;
import org.junit.Test;

import com.ract.ws.receipting.Message;
import com.ract.ws.receipting.ReceiptingResponse;

public class TestRoadside {
	RoadsideService service;
	Integer clientNo;
	
	@Before
	public void setUp() throws Exception {
		service = new RoadsideService();
		clientNo = 432002;
	}

	@Test
	public void testLineItems() {		
		RoadsideResponse response = service.getLineItems(clientNo, null);
		assertTrue(response.getStatus().equals(ReceiptingResponse.STATUS_SUCCESS));
		
		for (Payable payable : response.getLines()) { // check that each line remains valid
			response = service.getLineItems(clientNo, payable.getPayRefNo());
			assertTrue(response.getStatus().equals(ReceiptingResponse.STATUS_SUCCESS));
		}
	}
	
	@Test
	public void testValidateLines() {
		RoadsideResponse response = service.getLineItems(clientNo, null);
		assertTrue(response.getStatus().equals(ReceiptingResponse.STATUS_SUCCESS));
		
		Collection<Receipt> inputLines = new ArrayList<Receipt>();
		for (Payable payable : response.getLines()) {
			Receipt receipt = new Receipt();
			receipt.setClientNo(payable.getClientNo());
			receipt.setPayRefNo(payable.getPayRefNo());
			receipt.setAmountPaid(payable.getAmountOutstanding());
			
			inputLines.add(receipt);
		}
		
		response = service.validateLines(inputLines);
		assertTrue(response.getStatus().equals(ReceiptingResponse.STATUS_SUCCESS));
	}
	
	@Test
	public void testInvalidLines() {
		RoadsideResponse response = service.getLineItems(clientNo, null);
		assertTrue(response.getStatus().equals(ReceiptingResponse.STATUS_SUCCESS));
		
		Collection<Receipt> inputLines = new ArrayList<Receipt>();
		for (Payable payable : response.getLines()) {
			Receipt receipt = new Receipt();
			receipt.setClientNo(payable.getClientNo());
			receipt.setPayRefNo(payable.getPayRefNo());
			receipt.setAmountPaid(payable.getAmountOutstanding());
			
			inputLines.add(receipt);
		}
		Payable firstPayable = ((List<Payable>) response.getLines()).get(0);
		Receipt receipt = new Receipt();
		receipt.setClientNo(firstPayable.getClientNo());
		receipt.setPayRefNo("111111"); // doesn't exist
		receipt.setAmountPaid(firstPayable.getAmountOutstanding());
		inputLines.add(receipt);
		
		receipt = new Receipt();
		receipt.setClientNo(firstPayable.getClientNo());
		receipt.setPayRefNo(firstPayable.getPayRefNo());
		receipt.setAmountPaid(new BigDecimal(1)); // incorrect amount
		inputLines.add(receipt);
		
		response = service.validateLines(inputLines);
		assertTrue(response.getStatus().equals(ReceiptingResponse.STATUS_ERROR));
		
		for (Payable payable : response.getLines()) {
			for (Message msg : payable.getMessages()) {
				System.out.println(msg.toString());
				assertTrue(msg.getMessageType().equals(Message.TYPE_ERROR));
			}
		}
	}
	
	@Test
	public void testFinaliseLines() {
		RoadsideResponse response = service.getLineItems(clientNo, null);
		assertTrue(response.getStatus().equals(ReceiptingResponse.STATUS_SUCCESS));
		
		Collection<Receipt> inputLines = new ArrayList<Receipt>();
		for (Payable payable : response.getLines()) {
			Receipt receipt = new Receipt();
			receipt.setClientNo(payable.getClientNo());
			receipt.setPayRefNo(payable.getPayRefNo());
			receipt.setAmountPaid(new BigDecimal(1)); // incorrect amount
			receipt.setReceiptNo(123456);
			receipt.setReceiptedBy("gzn");
			
			inputLines.add(receipt);
		}
		
		response = service.finaliseLines(inputLines);
		assertTrue(response.getStatus().equals(ReceiptingResponse.STATUS_ERROR));
	}

}
