package com.ract.ws.receipting.roadside;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.*;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.SourceSystem;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.membership.MembershipDocument;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.TransactionMgr;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentMethod;
import com.ract.payment.PaymentMgr;
import com.ract.payment.billpay.PendingFee;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.ws.receipting.Message;
import com.ract.ws.receipting.ReceiptingResponse;

public class RoadsideService {
		
	public RoadsideService() {		
	}
	
	/**
	 * Retrieve payable items.
	 * If a payRefNo is provided, perform business validation.
	 * @param clientNo
	 * @param payRefNo
	 * @return
	 */
	public RoadsideResponse getLineItems(Integer clientNo, String payRefNo)  {
		LogUtil.info(this.getClass(), "getLineItems(clientNo=" + clientNo + ", payRefNo=" + payRefNo + ")");
		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		RoadsideResponse response = new RoadsideResponse();
		Collection<Payable> resultList = new ArrayList<Payable>();
		Map<String,String> clientDebts = new HashMap<String,String>(); 
		Payable payable = new Payable();
		String msg;
		DateTime validDate = new DateTime();
		Interval postExpireRenewalPeriod = null;
		
		try {
			postExpireRenewalPeriod = new Interval("0:6:0:0:0:0");
		} catch (ParseException e2) {}
		
		try {
			String postExpireRenewalPeriodSpec = CommonEJBHelper.getCommonMgrLocal().getSystemParameterValue(
					SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.POST_EXPIRE_RENEWAL_PERIOD);

	  	postExpireRenewalPeriod = new Interval(postExpireRenewalPeriodSpec);
	  	validDate = validDate.subtract(postExpireRenewalPeriod);
	  	
		} catch (Exception e1) {
			LogUtil.warn(getClass(), "Could not determine POST_EXPIRE_RENEWAL_PERIOD, defaulting to " + postExpireRenewalPeriod);
		}  	
				
		// Validate item about to be paid
		if (payRefNo != null) {
			try {
				payable = retrievePayable(payRefNo);
				if (payable == null) {
					msg = "No payable found for payRefNo: " + payRefNo + ", clientNo: " + clientNo;
					LogUtil.log(getClass(), msg);
					response.setStatus(ReceiptingResponse.STATUS_ERROR);
					payable = new Payable();
					payable.addMessage(Message.CODE_PAYABLE_NOT_FOUND, Message.TYPE_ERROR, msg);
				} else {
					clientNo = payable.getClientNo();
				}
				
				// check payable has amount outstanding
				if (payable.getAmountOutstanding() != null 
						&& (payable.getAmountOutstanding().compareTo(BigDecimal.ZERO) == -1 || payable.getAmountOutstanding().compareTo(BigDecimal.ZERO) == 0)) {
					msg = "No amount outstanding for payRefNo: " + payRefNo + ", clientNo: " + clientNo;
					LogUtil.log(getClass(), msg);
					response.setStatus(ReceiptingResponse.STATUS_ERROR);
					payable.addMessage(Message.CODE_PAYMENT_ALREADY_PAID, Message.TYPE_ERROR, msg);
				}
				
				resultList.add(payable);
				
			} catch (Exception e) {
				msg = e.getMessage() + " for payRefNo: " + payRefNo + ", clientNo: " + clientNo;
				LogUtil.log(getClass(), msg);
				response.setStatus(ReceiptingResponse.STATUS_ERROR);
				payable.addMessage(Message.CODE_OTHER, Message.TYPE_ERROR, msg);
				resultList.add(payable);
			}
			
			response.setLines(resultList);			
			return response;
		}
		
		// retrieve outstanding payables
		Collection<PayableItemVO> payableList = new ArrayList<PayableItemVO>();
		try {
			payableList = paymentMgr.getOutstandingPayableItemsForClient(SourceSystem.MEMBERSHIP, clientNo, PaymentMethod.RECEIPTING);			
		} catch (RemoteException e) {
			
		}
		
		for (PayableItemVO payableItemVO : payableList) {			
			try {
				payable = convertToPayable(payableItemVO);
				
				if (payable == null) {
					continue;
				}
				
				if (payableItemVO.getCreateDateTime().before(validDate)) {
					LogUtil.info(this.getClass(), "Skipping payable as it is too old: " + payableItemVO);
					continue;
				}
				
				resultList.add(payable);
				
				// store unique client debts here to prevent offering both pending and payable at the same time
				clientDebts.put(getDebtKey(payable), "true");
				
			} catch (Exception e) {
				/*payable = new Payable(payableItemVO.getDescription(), BigDecimal.ZERO, payableItemVO.getPayableItemID(), payableItemVO.getProductCode(), payableItemVO.getClientNo());
				payable.setStatus(Payable.STATUS_ERROR);
				payable.setMessage(e.getMessage());*/	
			}			
		}
		
		Collection<PendingFee> pendingList = new ArrayList<PendingFee>();
		try {
			pendingList = paymentMgr.getOutstandingPendingFeeByClientNumber(clientNo);
		} catch (RemoteException e) {
			
		}
		
		for (PendingFee pendingFee : pendingList) {			
			payable = convertToPayable(pendingFee);
			
			if (payable == null) {
				continue;
			}
			
			if (clientDebts.containsKey(getDebtKey(payable))) {
				LogUtil.info(this.getClass(), "Skipping pending fee as a matching payable was found: " + payable);
				continue; // skip pending, we already have a payable for same amount
			}
			
			resultList.add(payable);			
		}

		response.setLines(resultList);			
		return response;
	}
	
	/**
	 * Identify a debt by client and amount.
	 * @param payable
	 * @return
	 */
	private String getDebtKey(Payable payable) {
		return payable.getClientNo() + "," + payable.getAmountOutstanding().doubleValue();
	}
	
	/**
	 * Update source system with each receipt
	 * @param receiptList
	 * @return
	 */
	public RoadsideResponse finaliseLines(Collection<Receipt> receiptList) {

		RoadsideResponse response = new RoadsideResponse();
		Collection<Payable> resultList = new ArrayList<Payable>();
		String msg;
				
		for (Receipt receipt : receiptList) {
			LogUtil.info(this.getClass(), "finaliseLines(" + receipt + ")");
			String payRefNo = receipt.getPayRefNo();
			Integer clientNo = receipt.getClientNo();
			BigDecimal amountPaid = receipt.getAmountPaid();
			String receiptedBy = receipt.getReceiptedBy();
			receiptedBy = CommonConstants.USER_ROADSIDE;
			Integer receiptNumber = receipt.getReceiptNo();
				
			Payable payable = retrievePayable(payRefNo);
			
			if (payable == null) {
				msg = "No payable found for payRefNo: " + payRefNo + ", clientNo: " + clientNo + ", amountPaid: " + CurrencyUtil.formatDollarValue(amountPaid);
				LogUtil.log(getClass(), msg);
				response.setStatus(ReceiptingResponse.STATUS_ERROR);
				payable = new Payable();
				payable.addMessage(Message.CODE_PAYABLE_NOT_FOUND, Message.TYPE_ERROR, msg);
				resultList.add(payable);
				
			} else {			
				try {	
					
					if (receiptNumber == null || receiptNumber <= 0) {
						msg = "receiptNo is null for payRefNo: " + payRefNo + ", clientNo: " + clientNo + ", amountPaid: " + CurrencyUtil.formatDollarValue(amountPaid);
						LogUtil.log(getClass(), msg);
						throw new Exception(msg);
					}
					
					String receiptNo = receiptNumber.toString();
					
					TransactionMgr txnMgr = MembershipEJBHelper.getTransactionMgr();
					System.out.println("\n*** txnMgr: " + txnMgr.hashCode() + "\n");
					
					if (payable.isBpayIvr()) {	
						
						String sequenceNo = payRefNo.replace(Payable.BPAY_IVR_PREFIX, "");
						sequenceNo = sequenceNo.substring(0, sequenceNo.length()-1); // remove check-digit
						
						txnMgr.notifyPendingFeePaid(
							payRefNo, //in this case the pendingFeeId = IVR number
							Integer.parseInt(sequenceNo), // in this case the mem_document.document_id
		          amountPaid.doubleValue(),
		          payable.getAmountOutstanding().subtract(amountPaid).doubleValue(),
		          payable.getDescription(),
		          receiptedBy,
		          receiptNo);
						
			    } else {	
						txnMgr.notifyPayableFeePaid(
							payRefNo, 
		      		null, // not used 
		      		amountPaid.doubleValue(), 
		      		payable.getAmountOutstanding().subtract(amountPaid).doubleValue(), 
		      		payable.getDescription(), 
		      		receiptedBy,
		      		receiptNo);
			    }
					
					txnMgr.commit();
					
				} catch (Exception e) {
					// rollback occurs within notify... methods
					msg = e.getMessage() + " for payRefNo: " + payRefNo + ", clientNo: " + clientNo + ", amountPaid: " + CurrencyUtil.formatDollarValue(amountPaid);
					LogUtil.log(getClass(), msg);
					response.setStatus(ReceiptingResponse.STATUS_ERROR);
					payable.addMessage(Message.CODE_RECEIPT_ERROR, Message.TYPE_ERROR, msg);
					resultList.add(payable);
				}
			}
		}
		
		response.setLines(resultList);
		
		return response;
	}
	
	/**
	 * Validate the collection of line items. i.e. can they all be submitted?
	 * @param receiptList
	 * @return
	 */
	public RoadsideResponse validateLines(Collection<Receipt> receiptList) {

		RoadsideResponse response = new RoadsideResponse();
		Collection<Payable> resultList = new ArrayList<Payable>();
		Map<String,Boolean> uniqueRefs = new HashMap<String,Boolean>();
		String msg;
		
		for (Receipt receipt : receiptList) {
			LogUtil.info(this.getClass(), "validateLines(" + receipt + ")");
			Payable payable = retrievePayable(receipt.getPayRefNo());
			
			if (payable == null) {
				msg = "No payable found for payRefNo: " + receipt.getPayRefNo() + ", clientNo: " + receipt.getClientNo() + ", amountPaid: " + CurrencyUtil.formatDollarValue(receipt.getAmountPaid());
				LogUtil.log(getClass(), msg);
				response.setStatus(ReceiptingResponse.STATUS_ERROR);
				payable = new Payable();
				payable.setPayRefNo(receipt.getPayRefNo());
				payable.addMessage(Message.CODE_PAYABLE_NOT_FOUND, Message.TYPE_ERROR, msg);
				resultList.add(payable);
				
			} else if (uniqueRefs.containsKey(receipt.getPayRefNo())) {
				msg = "Multiple payments are not allowed for payRefNo:" + receipt.getPayRefNo() + ", clientNo: " + receipt.getClientNo() + ", amountPaid: " + CurrencyUtil.formatDollarValue(receipt.getAmountPaid());
				LogUtil.log(getClass(), msg);
				response.setStatus(ReceiptingResponse.STATUS_ERROR);
				payable.addMessage(Message.CODE_PAYMENT_ALREADY_PAID, Message.TYPE_ERROR, msg);
				resultList.add(payable);
				
			} else if (payable.getAmountOutstanding().compareTo(receipt.getAmountPaid()) != 0) { // not equivalent
				msg = "Partial payments are not allowed for payRefNo: " + receipt.getPayRefNo() + ", clientNo: " + receipt.getClientNo() + ", amountPaid: " + CurrencyUtil.formatDollarValue(receipt.getAmountPaid());
				LogUtil.log(getClass(), msg);
				response.setStatus(ReceiptingResponse.STATUS_ERROR);
				payable.addMessage(Message.CODE_PARTIAL_PAYMENT_NOT_ALLOWED, Message.TYPE_ERROR, msg);
				resultList.add(payable);
				
			} else {
				
				if (payable.isBpayIvr()) { // check not trying to pay pending, when payable outstanding
					MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
					try {
						List<MembershipVO> membershipList = (List<MembershipVO>) membershipMgr.findMembershipByClientNumber(payable.getClientNo());
						MembershipVO memVO = membershipList.get(0);
						if (memVO.isGroupMember()) {
							msg = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW_GROUP);
						} else {
							msg = memVO.getInvalidForTransactionReason(MembershipTransactionTypeVO.TRANSACTION_TYPE_RENEW);
						}
						
						//double recAmountUnpaid = memVO.getAmountOutstanding(true, false); // receipting=yes, dd=no
						//if (recAmountUnpaid > 0) {
						if (msg != null) {
			        msg += " for clientNo: " + receipt.getClientNo();
			        LogUtil.log(getClass(), msg);
			        response.setStatus(ReceiptingResponse.STATUS_ERROR);
							payable.addMessage(Message.CODE_OTHER, Message.TYPE_ERROR, msg);
							resultList.add(payable);
							continue;
						}
						
					} catch (Exception e) {
						msg = "Unable to get the total amount unpaid for clientNo: " + receipt.getClientNo();
						LogUtil.log(getClass(), msg);
						e.printStackTrace();
						response.setStatus(ReceiptingResponse.STATUS_ERROR);
						payable.addMessage(Message.CODE_OTHER, Message.TYPE_ERROR, msg);
						resultList.add(payable);
						continue;			
					}
				}
				
				uniqueRefs.put(receipt.getPayRefNo(), true);
				resultList.add(payable);
			}
		}
		
		response.setLines(resultList);
		
		return response;
	}
	
	/**
	 * Retrieve either a payableItem or pendingFee.
	 * @param payRefNo
	 * @return
	 */
	private Payable retrievePayable(String payRefNo) {

		PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
		Payable payable = null;
		
		PayableItemVO payableItem = null;
		PendingFee pendingFee = null;
		
		try {
			payableItem = paymentMgr.getPayableItem(Integer.parseInt(payRefNo));
		} catch (Exception e) {

		}
		
		if (payableItem == null) {				
			try {
				pendingFee = paymentMgr.getPendingFee(payRefNo);
			} catch (Exception e) {
	
			}
			if (pendingFee != null) {
				payable = convertToPayable(pendingFee);			
			}
			
		} else {
			try {
				payable = convertToPayable(payableItem);
			} catch (Exception e) {

			}
		}
		
		return payable;
	}

	/**
	 * Convert a PayableItem to a Payable.
	 * @param payableItem
	 * @return
	 */
	private Payable convertToPayable(PayableItemVO payableItem) {
		Payable payable = null;
		try {
			payable = new Payable(payableItem.getDescription(), new BigDecimal(payableItem.getAmountOutstanding()), payableItem.getPayableItemID().toString(), payableItem.getProductCode(), payableItem.getClientNo());
		} catch (ValidationException e) {

		}
		
		return payable;
	}

	/**
	 * Convert a PendingFee to a Payable.
	 * @param pendingFee
	 * @return
	 */
	private Payable convertToPayable(PendingFee pendingFee) {
		Payable payable = null;
		try {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			MembershipDocument document = membershipMgr.getMembershipDocument(pendingFee.getSeqNumber().intValue());
			
			if (document == null) {
				LogUtil.info(this.getClass(), "Skipping pending fee " + pendingFee.getPendingFeeID() + " as no document found: " + pendingFee.getSeqNumber().intValue());
				return null;
			}
			
			if (pendingFee.getFeeExpiryDate().before(new DateTime())) {
				LogUtil.info(this.getClass(), "Skipping pending fee " + pendingFee.getPendingFeeID() + " as it has expired: " + pendingFee.getFeeExpiryDate());
				return null;
			}
			
			/* 
			 * pending fee amount does not get updated following payment
			 * so we need to set it to zero if date paid is set.
			 */			
			BigDecimal amountOutstanding = BigDecimal.ZERO;
			if (pendingFee.getDatePaid() == null) {
				amountOutstanding = new BigDecimal(pendingFee.getAmount());
			}
			
			payable = new Payable(document.getDocumentTypeCode(), amountOutstanding, pendingFee.getPendingFeeID(), document.getProductCode(), pendingFee.getClientNumber());
		} catch (Exception e) {

		}		
		
		return payable;
	}
}
