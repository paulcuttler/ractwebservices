package com.ract.ws.core.client;

import java.util.Collection;
import java.util.Date;

import javax.jws.WebParam;

import com.ract.client.*;
import com.ract.util.DateTime;

@javax.jws.WebService(targetNamespace = "http://client.core.ws.ract.com/", serviceName = "CoreClientService", portName = "CoreClientPort")
public class ClientServiceDelegate {

	com.ract.ws.core.client.ClientService clientService = new com.ract.ws.core.client.ClientService();
	
	public Long getClientChangesCount(
			@WebParam(name = "startDate") Date start, 
			@WebParam(name = "endDate") Date end) {
		return clientService.getClientChangesCount(start, end);
	}
	
	public Collection<Integer> getClientChangesIdentifiers(
			@WebParam(name = "startDate") Date start, 
			@WebParam(name = "endDate") Date end) {
		return clientService.getClientChangesIdentifiers(start, end);
	}
	
	public Collection<Client> getClientChanges(
			@WebParam(name = "startDate") Date start, 
			@WebParam(name = "endDate") Date end) {
		return clientService.getClientChanges(start, end);
	}

	public Boolean isClientChangesExist(
			@WebParam(name = "startDate") Date start, 
			@WebParam(name = "endDate") Date end) {
		return clientService.isClientChangesExist(start, end);
	}
	
	public Client getClientById(@WebParam(name = "personNumber") Integer personNumber) {
		return clientService.getClientById(personNumber);
	}
	
	public Integer updateClient(@WebParam(name = "client") Client client, @WebParam(name = "sourceSystem") String sourceSystem) {
		return clientService.updateClient(client, sourceSystem);
	}
	
	public Collection<String> validateClientData(@WebParam(name = "client") Client client) {
		return clientService.validateClientData(client);
	}
}