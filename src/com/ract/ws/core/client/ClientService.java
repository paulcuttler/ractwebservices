package com.ract.ws.core.client;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.ws.WebServiceException;

import com.ract.client.*;
import com.ract.membership.importer.ImportMgrBean;
import com.ract.security.SecurityHelper;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.common.CommonEJBHelper;
import com.ract.common.GenericException;
import com.ract.common.MailMgr;
import com.ract.common.SourceSystem;
import com.ract.common.StreetSuburbVO;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.common.mail.MailMessage;
import com.ract.common.qas.QASAddress;
import com.ract.common.streetsuburb.StreetSuburbFactory;

public class ClientService {
	
	static Collection<String> excludedSystems;
	static Collection<String> clientStatus;
	
	static {
		excludedSystems = new ArrayList<String>();
		excludedSystems.add(SourceSystem.MRM.getAbbreviation());
		
		clientStatus = new ArrayList<String>();
		clientStatus.add(com.ract.client.Client.STATUS_ACTIVE);
		clientStatus.add(com.ract.client.Client.STATUS_ADDRESS_UNKNOWN);
		clientStatus.add(com.ract.client.Client.STATUS_DECEASED);
		clientStatus.add(com.ract.client.Client.STATUS_DUPLICATE);
		clientStatus.add(com.ract.client.Client.STATUS_INACTIVE);
		clientStatus.add("INVALID"); // for backwards compatibility
	}
	
	/**
	 * Returns the number of client changes between the start and end times. 
	 * This will be used by an overnight process to ensure all reported changes have made the transition to the MRM.
	 * @param start
	 * @param end
	 * @return
	 * @throws WebServiceException
	 */
	public Long getClientChangesCount(Date start, Date end) {
		LogUtil.debug(this.getClass(), "getClientChangesCount(start=" + start + ", end=" + end + ")");
		
		Long count = (long) 0;

		if (start == null) {
			throw new WebServiceException("Start date cannot be null");
		}
		if (end == null) {
			throw new WebServiceException("End date cannot be null");
		}		
		if (end.before(start)) {
			throw new WebServiceException("End date cannot be before start date");
		}
				
		try {			
			ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
			count = clientMgr.getClientChangesCount(new DateTime(start), new DateTime(end), excludedSystems);
			
		} catch (Exception e) {
			LogUtil.fatal(getClass(), "getClientChangesCount() failed: " + e);
			throw new WebServiceException(e.getMessage());
		}
				
		LogUtil.debug(this.getClass(), "getClientChangesCount() found " + count + " client changes for range " + start.toString() + " - " + end.toString());
		
		return count;
	}
	
	/**
	 * Return the identifiers of all clients changed during the date range. 
   * Used in an overnight process in conjunction with the @getClientChangesCount web method. 
   * If a missing client is found, force a sync on that client.
	 * @param start
	 * @param end
	 * @return
	 * @throws WebServiceException
	 */
	public Collection<Integer> getClientChangesIdentifiers(Date start, Date end) {
		LogUtil.debug(this.getClass(), "getClientChangesIdentifiers(start=" + start + ", end=" + end + ")");
		
		Collection<Integer> idList =  new ArrayList<Integer>();
		
		if (start == null) {
			throw new WebServiceException("Start date cannot be null");
		}
		if (end == null) {
			throw new WebServiceException("End date cannot be null");
		}
		if (end.before(start)) {
			throw new WebServiceException("End date cannot be before start date");
		}
		
		try {
			ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
			idList = clientMgr.getClientChangesIdentifiers(new DateTime(start), new DateTime(end), excludedSystems);
			
		} catch (Exception e) {
			LogUtil.fatal(getClass(), "getClientChangesIdentifiers() failed: " + e);
			throw new WebServiceException(e.getMessage());
		}
				
		LogUtil.debug(this.getClass(), "getClientChangesIdentifiers() found " + idList.size() + " client changes for range " + start.toString() + " - " + end.toString());
		
		return idList;
	}
	
	/**
	 * Returns array of Client objects that have been created or modified between the start and end dates (inclusive). 
	 * Will not return rows that were most recently updated by the MRM transactional sync component.
	 * @param start
	 * @param end
	 * @return
	 * @throws WebServiceException
	 */
	public Collection<Client> getClientChanges(Date start, Date end) {  
		LogUtil.debug(this.getClass(), "getClientChanges(start=" + start + ", end=" + end + ")");
		
		boolean excludeGroupsFromClientWS = false;
		try {
			excludeGroupsFromClientWS = Boolean.parseBoolean(FileUtil.getProperty("master", "excludeGroupsFromClientWS"));
		} catch (Exception e) {
			LogUtil.warn(getClass(), "Could not determine excludeGroupsFromClientWS, defaulting to: " + excludeGroupsFromClientWS);
		}
		
		Collection<ClientVO> clientVoList = new ArrayList<ClientVO>();
		Collection<Client> clientList = new ArrayList<Client>();
		Client client = null;
		
		if (start == null) {
			throw new WebServiceException("Start date cannot be null");
		}
		if (end == null) {
			throw new WebServiceException("End date cannot be null");
		}
		if (end.before(start)) {
			throw new WebServiceException("End date cannot be before start date");
		}
		
		try {
			ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
			clientVoList = clientMgr.getClientChanges(new DateTime(start), new DateTime(end), excludedSystems);
			
		} catch (Exception e) {
			LogUtil.fatal(getClass(), "getClientChanges() failed: " + e);
			throw new WebServiceException(e);
		}
		
		for (ClientVO clientVO : clientVoList) {
			try {
				client = new Client(clientVO);
				
				if (!excludeGroupsFromClientWS) {
					client.setOwnedGroups(this.getOwnedGroups(clientVO.getClientNumber()));
					
					if (client.getGroupClient()) {
						client.setGroupMembers(this.getGroupMembers(clientVO.getClientNumber()));
					}
				}
				
				clientList.add(client); // translate ClientVO to Client
			} catch (Exception e) {
				LogUtil.warn(this.getClass(), "unable to add client(" + clientVO.getClientNumber() + ") to list: " + e.getMessage());
			}
		}
				
		LogUtil.info(this.getClass(), "getClientChanges() found " + clientList.size() + " client changes for range " + start.toString() + " - " + end.toString());
		
		return clientList;
	}
	
	/**
	 * Returns boolean; true if changes (creates/updates) exist between the entered dates, false otherwise.
	 * @param start
	 * @param end
	 * @return
	 * @throws SystemException
	 */
	public Boolean isClientChangesExist(Date start, Date end) {
		LogUtil.debug(this.getClass(), "isClientChangesExist(start=" + start + ", end=" + end + ")");
		
		boolean exists = false;

		if (start == null) {
			throw new WebServiceException("Start date cannot be null");
		}
		if (end == null) {
			throw new WebServiceException("End date cannot be null");
		}
		if (end.before(start)) {
			throw new WebServiceException("End date cannot be before start date");
		}
		
		try {
			ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
			exists = clientMgr.isClientChangesExist(new DateTime(start), new DateTime(end), excludedSystems);
			
		} catch (Exception e) {
			LogUtil.fatal(getClass(), "isClientChangesExist() failed: " + e);
			throw new WebServiceException(e.getMessage());
		}
		
		LogUtil.debug(this.getClass(), "isClientChangesExist() " + (exists ? "found" : "did not find") + " client changes for range " + start.toString() + " - " + end.toString());
		
		return exists;
	}
	
	/**
	 * Returns single Client object.
	 * @param personNumber
	 * @return
	 * @throws ValidationException
	 * @throws RemoteException
	 */
	public Client getClientById(Integer personNumber) {
		LogUtil.info(this.getClass(), "getClientById(personNumber=" + personNumber+ ")");
		
		boolean excludeGroupsFromClientWS = false;
		try {
			excludeGroupsFromClientWS = Boolean.parseBoolean(FileUtil.getProperty("master", "excludeGroupsFromClientWS"));
		} catch (Exception e) {
			LogUtil.warn(getClass(), "Could not determine excludeGroupsFromClientWS, defaulting to: " + excludeGroupsFromClientWS);
		}
		
		Client client = null;
		
		if (personNumber == null) {
			throw new WebServiceException("Person number cannot be null");
		}
		
		try {
			ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
			ClientVO clientVO = clientMgr.getClient(personNumber);
			if (clientVO == null) {
				throw new Exception("No client found for personNumber: " + personNumber);
			}
			
			client = new Client(clientVO);
			
			if (!excludeGroupsFromClientWS) {
				client.setOwnedGroups(this.getOwnedGroups(personNumber));
					
				if (client.getGroupClient()) {
					client.setGroupMembers(this.getGroupMembers(personNumber));
				}
			}
						
		} catch (Exception e) {
			LogUtil.fatal(getClass(), e);
			throw new WebServiceException(e.getMessage());
		}
		
		return client;
	}
	
	/**
	 * Lookup insurance groups that this client is part of.
	 * @param personNumber
	 * @return
	 * @throws FaultDetailMessage
	 * @throws SystemException 
	 */
	private Collection<Integer> getOwnedGroups(Integer personNumber) throws SystemException {
		Collection<Integer> ownedGroups = new ArrayList<Integer>();
		
		ClientAdapter clientAdapter = ClientFactory.getClientAdapter();
		ownedGroups = clientAdapter.getOwnedGroups(personNumber);
				
		return ownedGroups;	
	}
	
	/**
	 * Lookup members of this group client.
	 * @param personNumber
	 * @return
	 * @throws FaultDetailMessage
	 * @throws SystemException 
	 */
	private Collection<Integer> getGroupMembers(Integer personNumber) throws SystemException {
		Collection<Integer> groupMembers = new ArrayList<Integer>();
		
		ClientAdapter clientAdapter = ClientFactory.getClientAdapter();
		Collection<ClientVO> members = clientAdapter.getClientGroup(personNumber); 
		for (ClientVO p : members) {
			groupMembers.add(p.getClientNumber());
		}
		
		//LogUtil.info(this.getClass(), "group members: " + groupMembers.size());
				
		return groupMembers;	
	}
	
	/**
	 * Returns Id of record in cl-master.
	 * Updates client if it exists, creates new client otherwise.
	 * Ideally call would not return until record is in both Roadside and Insurance DBs.
	 * @param clientData
	 * @param sourceSystem
	 * @return
	 */
	public Integer updateClient(Client clientData, String sourceSystem) {
		LogUtil.info(getClass(), "updateClient(" + clientData + ", " + sourceSystem + ")");
		
		// convert Client to ClientVO
		String userId = null;
		String salesBranchCode = "HOB";
		Integer clientNumber = null;		
		
		if (clientData == null) {
			throw new WebServiceException("Client cannot be null");
		}
		
		if (sourceSystem == null || sourceSystem.isEmpty()) {
			throw new WebServiceException("Source System cannot be null");
		}
		
		if (!sourceSystem.equals(SourceSystem.PROGRESS_CLIENT.getAbbreviation())) {
			Collection<String> errors = validateClientData(clientData);
			if (errors != null && !errors.isEmpty()) {
				throw new WebServiceException("Client minimum data requirements not met: " + errors);
			}
		} else { // skip validation for Putty
			LogUtil.info(getClass(), "updateClient() bypassing validation for " + sourceSystem);
		}
		
		try {
			ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
			userId = SecurityHelper.getSystemUser().getUserID();
		
			// get client
			ClientVO clientVO = null;
			if (clientData.getClientNumber() != null && clientData.getClientNumber() != 0) {
				clientVO = clientMgr.getClient(clientData.getClientNumber());
			} else {
				throw new Exception("ClientNumber cannot be 0!");
			}
			ClientVO newClientVO = translateClient(clientData, clientVO, sourceSystem);
			
			// client exists
			if (clientVO != null) {	// update
				newClientVO = clientMgr.updateClient(newClientVO, userId, com.ract.client.Client.HISTORY_UPDATE, salesBranchCode, "Updated client via " + sourceSystem, sourceSystem);
			} else { // create
				newClientVO = clientMgr.createClient(newClientVO, userId, com.ract.client.Client.HISTORY_CREATE, salesBranchCode, "Created client via " + sourceSystem, sourceSystem);
			}
			
			clientNumber = newClientVO.getClientNumber();
			
		} catch (Exception e) {
			LogUtil.fatal(getClass(), e);

	    	String emailTo = "g.newton@ract.com.au";
	    	try {
	    		emailTo = FileUtil.getProperty("master", "updateClientFailureAlert");
	    	} catch (Exception ex) {
	    		LogUtil.warn(getClass(), "Could not determine updateClientFailureAlert, defaulting to: " + emailTo);
	    	}
		  
	    	String systemName = "Unknown";
	    	try {
	    		systemName = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.TYPE_SYSTEM);
	    	} catch (RemoteException r) {}
		  
	    	MailMessage message = new MailMessage();
	    	message.setRecipient(emailTo);
	    	message.setSubject(systemName + " (" + sourceSystem + "): updateClient failed! " + clientData.getClientNumber());
	    	message.setMessage(clientData + "\n\n" + e.getMessage());
	    	MailMgr mailMgr = CommonEJBHelper.getMailMgr();      
	    	      
	      	try {
				mailMgr.sendMail(message);
	      	
	      	} catch(Exception ex) {
	      		LogUtil.fatal(this.getClass(), "UNABLE TO EMAIL updateClient FAILURE NOTICE!: " + e);
	      		e.printStackTrace();
		    }
			
			//e.printStackTrace();
			throw new WebServiceException(e.getMessage());
		}
		
		return clientNumber;
	}
	
	/**
	 * Translate Client to ClientVO.
	 * @param clientData
	 * @param clientVO
	 * @return
	 */
	private ClientVO translateClient(Client clientData, ClientVO clientVO, String sourceSystem) throws Exception {
		
		Integer resiStsubId = null;
		Integer postStsubId = null;
		String systemUserId = SecurityHelper.getSystemUser().getUserID();
		
		if (clientVO == null) {
			clientVO = new ClientVO();
			clientVO.setClientNumber(clientData.getClientNumber());
			clientVO.setMadeID(systemUserId);
			clientVO.setStatus(com.ract.client.Client.STATUS_ACTIVE);
			clientVO.setMadeDate(new DateTime());
			clientVO.setSmsAllowed(true);
			clientVO.setMotorNewsDeliveryMethod(com.ract.client.Client.MOTOR_NEWS_DELIVERY_METHOD_NORMAL);
			clientVO.setMotorNewsSendOption(com.ract.client.Client.MOTOR_NEWS_DELIVERY_METHOD_NORMAL);
		} else {
			clientVO.setStatus(clientData.getStatus());
		}
		
		clientVO.setLastUpdate(new DateTime());
		
		if (clientData.getDateOfBirth() != null && !clientData.getDateOfBirth().isEmpty()) {
			if (clientData.getDateOfBirth().equalsIgnoreCase(ClientVO.FIELD_STATUS_REFUSED) || clientData.getDateOfBirth().equalsIgnoreCase(ClientVO.FIELD_STATUS_TO_FOLLOW)) {
				clientVO.setBirthDateString(clientData.getDateOfBirth());
				clientVO.setBirthDate(null);
			} else {
				try {
					clientVO.setBirthDateString(null);
					clientVO.setBirthDate(new DateTime(clientData.getDateOfBirth()));
				} catch (Exception e) {}
			}
		} else {
			clientVO.setBirthDate(null);
			clientVO.setBirthDateString(null);
		}
		
		clientVO.setEmailAddress(clientData.getEmail() == null || clientData.getEmail().isEmpty() ? ClientVO.FIELD_STATUS_NOT_APPLICABLE : clientData.getEmail());
		
		if (clientData.getGender() != null) {
			if (clientData.getGender().equalsIgnoreCase("Male")) {
				clientVO.setSex(true);
			} else if (clientData.getGender().equalsIgnoreCase("Female")) {
				clientVO.setSex(false);
			} // otherwise null, e.g. Organisation
		}
		
		StringBuilder givenNames = new StringBuilder();
		givenNames.append(clientData.getFirstName() != null ? clientData.getFirstName().trim() : "");
		if (clientData.getMiddleName() != null && !clientData.getMiddleName().isEmpty()) {
			givenNames.append(clientData.getFirstName() != null && !clientData.getFirstName().isEmpty() ? " " : "");
			givenNames.append(clientData.getMiddleName().trim());
		}		
		clientVO.setGivenNames(givenNames.toString());
		
		clientVO.setGroupClient(clientData.getGroupClient());		
		clientVO.setHomePhone(clientData.getHomePhone());
		clientVO.setMobilePhone(clientData.getMobilePhone());
		clientVO.setSurname(clientData.getLastName());
		clientVO.setTitle(clientData.getTitle());
		clientVO.setWorkPhone(clientData.getWorkPhone());
		clientVO.setABN(clientData.getABN());
		
		if (sourceSystem.equals(SourceSystem.PROGRESS_CLIENT.getAbbreviation())) {
			clientVO.setMasterClientNumber(clientData.getMasterClientNumber());
			clientVO.setPhonePassword(clientData.getPhonePassword());
			clientVO.setPhoneQuestion(clientData.getPhoneQuestion());
			clientVO.setContactPersonName(clientData.getContactPersonName());
			clientVO.setContactPersonPhone(clientData.getContactPersonPhone());
		}
				
		// lookup Residential StreetSuburb
	    String resiStreet = clientData.getResidentialStreetName();
	    resiStreet = abbreviateStreetName(resiStreet);
	    
	    String resiState = clientData.getResidentialState();
	    resiState = abbreviateState(resiState);
	    
	    String resiSuburb = clientData.getResidentialSuburb();
	    if (resiSuburb != null) {
	    	resiSuburb = resiSuburb.trim();
	    }
	    
	    String resiCountry = clientData.getResidentialCountry();
	    if (resiCountry != null) {
	    	resiCountry = resiCountry.trim();
	    }
	    
	    if (!resiCountry.equalsIgnoreCase(Client.DEFAULT_COUNTRY)
	    		&& !resiCountry.equalsIgnoreCase(Client.DEFAULT_COUNTRY_CODE)
	    		&& !resiCountry.equalsIgnoreCase(Client.DEFAULT_COUNTRY_CODE_ALT)) {
	    	clientData.setResidentialState(""); // International, suppress state
	    }
	    List<StreetSuburbVO> ssList = CommonEJBHelper.getCommonMgr().getStreetSuburbList(
				resiStreet, resiSuburb, resiState);
    		
		if (!ssList.isEmpty()) {
			if (ssList.size() > 1) {
				// if multiple, match postcode
				for (StreetSuburbVO ssVO : ssList) {
					if (clientData.getResidentialPostcode() != null && ssVO.getPostcode().equals(clientData.getResidentialPostcode().toString())) {
						resiStsubId = ssVO.getStreetSuburbID();
						break;
					}
				}
			}
			if (resiStsubId == null) {
				resiStsubId = ssList.get(0).getStreetSuburbID();
			}			
		} else {
			// create street/suburb
			QASAddress q = new QASAddress();
			if (clientData.getResidentialPostcode() != null) {
				q.setPostcode(clientData.getResidentialPostcode().toString());
			}
			q.setStreet(resiStreet.toUpperCase());
			q.setSuburb(resiSuburb.toUpperCase());
			q.setState(resiState.toUpperCase());
			// @todo q.setCountry(resiCountry.toUpperCase())
			q.setUserid(systemUserId);
			try {
				LogUtil.info(getClass(), "Creating new Residential StreetSuburb: " + q);
				resiStsubId = StreetSuburbFactory.getStreetSuburbAdapter().createProgressStreetSuburb(q);
  			
			} catch (Exception e) {
				throw new GenericException("Cannot create Residential Street/Suburb: " + resiStreet + ", " + resiSuburb + ": " + e);
			}  	
		}
				
		clientVO.setResiDpid(clientData.getResidentialDpid());
		clientVO.setResiProperty(clientData.getResidentialProperty());
		clientVO.setResiStreetChar(clientData.getResidentialStreetNumber());		
		clientVO.setResiStsubId(resiStsubId);		
		clientVO.setResiGnaf(clientData.getResidentialGnaf());
		clientVO.setResiLatitude(clientData.getResidentialLatitude());
		clientVO.setResiLongitude(clientData.getResidentialLongitude());
		clientVO.setResiAusbar(clientData.getResidentialAusbar());
	    clientVO.setResiDpid(clientData.getResidentialDpid());
		
		// lookup Postal StreetSuburb
	    String postalStreet = clientData.getPostalStreetName();
	    postalStreet = abbreviateStreetName(postalStreet);
	    
	    String postalState = clientData.getPostalState();
	    postalState = abbreviateState(postalState);

	    String postalSuburb = clientData.getPostalSuburb();
	    if (postalSuburb != null) {
	    	postalSuburb = postalSuburb.trim();
	    }
	    
	    String postalCountry = clientData.getPostalCountry();
	    if (postalCountry != null) {
	    	postalCountry = postalCountry.trim();
	    }
    
	    if (!postalCountry.equalsIgnoreCase(Client.DEFAULT_COUNTRY)
	    		&& !postalCountry.equalsIgnoreCase(Client.DEFAULT_COUNTRY_CODE)
	    		&& !postalCountry.equalsIgnoreCase(Client.DEFAULT_COUNTRY_CODE_ALT)) {
	    	clientData.setPostalState(""); // International, suppress state
	    }
	    ssList = CommonEJBHelper.getCommonMgr().getStreetSuburbList(
				postalStreet, postalSuburb, postalState);
    
		if (!ssList.isEmpty()) {
			if (ssList.size() > 1) {
				// if multiple, match postcode
				for (StreetSuburbVO ssVO : ssList) {
					if (clientData.getPostalPostcode() != null && ssVO.getPostcode().equals(clientData.getPostalPostcode().toString())) {
						postStsubId = ssVO.getStreetSuburbID();
						break;
					}
				}
			}
			if (postStsubId == null) {
				postStsubId = ssList.get(0).getStreetSuburbID();
			}
		} else {
			// create street/suburb
			QASAddress q = new QASAddress();
			if (clientData.getPostalPostcode() != null) {
				q.setPostcode(clientData.getPostalPostcode().toString());
			}
			q.setStreet(postalStreet.toUpperCase());
			q.setSuburb(postalSuburb.toUpperCase());
			q.setState(postalState.toUpperCase());
			// @todo q.setCountry(postalCountry.toUpperCase())
			q.setUserid(systemUserId);
			try {
				LogUtil.info(getClass(), "Creating new Postal StreetSuburb: " + q);
				postStsubId = StreetSuburbFactory.getStreetSuburbAdapter().createProgressStreetSuburb(q);
  			
			} catch (Exception e) {
				throw new GenericException("Cannot create Postal Street/Suburb: " + postalStreet + ", " + postalSuburb + ": " + e);
			}
		}
		
		clientVO.setPostDpid(clientData.getPostalDpid());
		clientVO.setPostProperty(clientData.getPostalProperty());
		clientVO.setPostStreetChar(clientData.getPostalStreetNumber());				
		clientVO.setPostStsubId(postStsubId);
		clientVO.setPostDpid(clientData.getPostalDpid());		
		clientVO.setPostGnaf(clientData.getPostalGnaf());
		clientVO.setPostLatitude(clientData.getPostalLatitude());
		clientVO.setPostLongitude(clientData.getPostalLongitude());
		clientVO.setPostAusbar(clientData.getPostalAusbar());
		clientVO.setPostalName(clientData.getPostalName());
		
		clientVO.setDerivedFields();
		
		return clientVO;
	}
	
	/**
	 * Validate provided client data meets minimum requirements.
	 * @param clientData
	 */
	public Collection<String> validateClientData(Client clientData) {
		
		Collection<String> errors = new ArrayList<String>();
		
		if (clientData.getClientType() == null || clientData.getClientType().isEmpty()) {
			errors.add("ClientType cannot be blank");
		}
		
		if (clientData.getStatus() == null || clientData.getStatus().isEmpty()) {
			errors.add("Status cannot be blank");
		} else {
			boolean statusOk = false;
			for (String status : clientStatus) {
				if (status.equalsIgnoreCase(clientData.getStatus())) {
					statusOk = true;
					break;
				}
			}
			
			if (!statusOk) {
				errors.add("Status is not valid");
			}
		}
		
		if (clientData.getClientType().equalsIgnoreCase(ClientVO.CLIENT_TYPE_PERSON)) {
			/*if (clientData.getTitle() == null || clientData.getTitle().isEmpty()) {
				errors.add("Title cannot be blank");
			}*/
			
			try {
				validateDateOfBirth(clientData.getDateOfBirth());
			} catch (Exception e) {
				errors.add(e.toString());
			}
			
			if (clientData.getGroupClient() == null || !clientData.getGroupClient()) {
				if (clientData.getGender() == null || clientData.getGender().isEmpty()) {
					errors.add("Gender cannot be blank");
				}				
			}
			
			if (clientData.getFirstName() == null || clientData.getFirstName().isEmpty()) {
				errors.add("FirstName cannot be blank");
			}
			
			try {
				validatePhoneNumber(clientData, NumberUtil.PHONE_TYPE_HOME);
			} catch (Exception e) {
				errors.add(e.toString());
			}
		}
				
		if (clientData.getGroupClient() == null) {
			errors.add("GroupClient cannot be blank");
		}
		
		if (clientData.getLastName() == null || clientData.getLastName().isEmpty()) {
			errors.add("LastName cannot be blank");
		}
		
		try {
			validatePhoneNumber(clientData, NumberUtil.PHONE_TYPE_WORK);
		} catch (Exception e) {
			errors.add(e.toString());
		}

		try {
			validatePhoneNumber(clientData, NumberUtil.PHONE_TYPE_MOBILE);
		} catch (Exception e) {
			errors.add(e.toString());
		}
		
		if (clientData.getClientType().equalsIgnoreCase(ClientVO.CLIENT_TYPE_PERSON)) {
			if (clientData.getResidentialStreetNumber() == null || clientData.getResidentialStreetNumber().isEmpty()) {
				errors.add("ResidentialStreetNumber cannot be blank");
			}
			if (clientData.getResidentialStreetName() == null || clientData.getResidentialStreetName().isEmpty()) {
				errors.add("ResidentialStreetName cannot be blank");
			}
		} else { // Organisation - Residential address can be a postal form
			if (clientData.getResidentialProperty() == null || clientData.getResidentialProperty().isEmpty()) {
				boolean hasStreetNo = false;
				if (clientData.getResidentialStreetNumber() == null || clientData.getResidentialStreetNumber().isEmpty()) {
					errors.add("ResidentialStreetNumber cannot be blank");
				} else {
					hasStreetNo = true;
				}
				
				boolean hasStreetName = false;
				if (clientData.getResidentialStreetName() == null || clientData.getResidentialStreetName().isEmpty()) {
					errors.add("ResidentialStreetName cannot be blank");
				} else {
					hasStreetName = true;
				}

				if (hasStreetNo && hasStreetName) {
					// ok
				} else {
					errors.add("ResidentialProperty cannot be blank");
				}
			}
		}
		
		if (clientData.getResidentialSuburb() == null || clientData.getResidentialSuburb().isEmpty()) {
			errors.add("ResidentialSuburb cannot be blank");
		}
		if (clientData.getResidentialState() == null || clientData.getResidentialState().isEmpty()) {
			errors.add("ResidentialState cannot be blank");
		}
		
		if (clientData.getPostalProperty() == null || clientData.getPostalProperty().isEmpty()) {
			boolean hasStreetNo = false;
			if (clientData.getPostalStreetNumber() == null || clientData.getPostalStreetNumber().isEmpty()) {
				errors.add("PostalStreetNumber cannot be blank");
			} else {
				hasStreetNo = true;
			}
			
			boolean hasStreetName = false;
			if (clientData.getPostalStreetName() == null || clientData.getPostalStreetName().isEmpty()) {
				errors.add("PostalStreetName cannot be blank");
			} else {
				hasStreetName = true;
			}

			if (hasStreetNo && hasStreetName) {
				// ok
			} else {
				errors.add("PostalProperty cannot be blank");
			}
		}
		
		if (clientData.getPostalSuburb() == null || clientData.getPostalSuburb().isEmpty()) {
			errors.add("PostalSuburb cannot be blank");
		}
		if (clientData.getPostalState() == null || clientData.getPostalState().isEmpty()) {
			errors.add("PostalState cannot be blank");
		}
				
		return errors;
	}
	
	/**
	 * Validate a DOB.
	 * @param dateOfBirth
	 * @throws ValidationException
	 */
	private void validateDateOfBirth(String dateOfBirth) throws ValidationException {
		if (dateOfBirth == null || dateOfBirth.isEmpty()) {
			throw new ValidationException("DateOfBirth cannot be blank");
		} else if (!dateOfBirth.equalsIgnoreCase(ClientVO.FIELD_STATUS_REFUSED) && !dateOfBirth.equalsIgnoreCase(ClientVO.FIELD_STATUS_TO_FOLLOW)) {
			try {
				DateTime dob = new DateTime(dateOfBirth);
				if (dob.after(new DateTime())) {
					throw new ValidationException("DateOfBirth cannot be in the future: " + dateOfBirth);
				}				
			} catch (Exception e) {
				throw new ValidationException("DateOfBirth is not a valid format: " + dateOfBirth);
			}
		}
	}
	
	/**
	 * Validate a phone number.
	 * Pilfered from master:SaveClientAction
	 * @param clientData
	 * @param phoneType
	 * @throws ValidationException
	 */
	private void validatePhoneNumber(Client clientData, String phoneType) throws ValidationException {
  	String phoneNumber = null;
  	if (phoneType.equals(NumberUtil.PHONE_TYPE_HOME)) {
  		phoneNumber = clientData.getHomePhone(); 
  	} else if (phoneType.equals(NumberUtil.PHONE_TYPE_WORK)) {
  		phoneNumber = clientData.getWorkPhone();
  	} else if (phoneType.equals(NumberUtil.PHONE_TYPE_MOBILE)) {
  		phoneNumber = clientData.getMobilePhone();
  	} else {
  		throw new ValidationException("Unknown phone type: " + phoneType);
  	}
  			
		try {
			if (phoneNumber != null && !phoneNumber.isEmpty()) {
				if (phoneNumber.equalsIgnoreCase(ClientVO.FIELD_STATUS_NOT_APPLICABLE) ||
				    phoneNumber.equalsIgnoreCase(ClientVO.FIELD_STATUS_REFUSED) ||
				    phoneNumber.equalsIgnoreCase(ClientVO.FIELD_STATUS_TO_FOLLOW)) {
					LogUtil.debug(this.getClass(), "phoneNumber=" + phoneNumber + " is valid");
					//valid
				} else {
					LogUtil.debug(this.getClass(), "phoneNumber=" + phoneNumber);
				  NumberUtil.validatePhoneNumber(phoneType, phoneNumber);
				}
			} else {
				throw new ValidationException(phoneType + " phone number must be entered");
			}
		} catch (Exception e)	{
			throw new ValidationException("Unable to validate phone number: " + phoneNumber + " : " + e.getMessage());
		}	
  }
	
	/**
	 * Attempt to abbreviate street type.
	 * @param streetName
	 * @return
	 */
	public String abbreviateStreetName(String streetName) {
		if (streetName == null || streetName.isEmpty()) {
			return "";
		}
		
		// strip commas
		streetName = streetName.replaceAll(",", "");
		
		if (streetName.toUpperCase().startsWith("THE ")) { // e.g. THE BOULEVARD, THE CRESCENT - don't abbreviate
			return streetName;
		}
		
		streetName = streetName.replaceAll("\\s+", " "); // reduce multiple spaces to single
	   	String[] parts = streetName.split(" ");
	   	
	   	if (parts.length == 1) { // e.g. Esplanade, don't abbreviate
	   		return streetName;
	   	}
	   	
	   	if (ImportMgrBean.streetTypeAbbr.containsKey(parts[parts.length-1].toUpperCase())) {
 			parts[parts.length-1] = ImportMgrBean.streetTypeAbbr.get(parts[parts.length-1].toUpperCase());
 		}
	   	
	   	StringBuffer abbrev = new StringBuffer();
	   	for (String s : parts) {
	   		abbrev.append(s).append(" ");
	   	}
	   	
	   	return abbrev.toString().trim();
	}
	
	/**
	 * Attempt to abbreviate State.
	 * @param stateName
	 * @return
	 */
	public String abbreviateState(String stateName) {
		if (stateName == null || stateName.isEmpty()) {
			return "";
		}
		
		if (stateName.trim().length() <= 3) {
			return stateName;
		}
		
		if (stateName.trim().equalsIgnoreCase("TASMANIA")) {
			return "TAS";
		} else if (stateName.trim().equalsIgnoreCase("VICTORIA")) {
			return "VIC";
		} else if (stateName.trim().equalsIgnoreCase("NEW SOUTH WALES")) {
			return "NSW";
		} else if (stateName.trim().equalsIgnoreCase("QUEENSLAND")) {
			return "QLD";
		} else if (stateName.trim().equalsIgnoreCase("SOUTH AUSTRALIA")) {
			return "SA";
		} else if (stateName.trim().equalsIgnoreCase("NORTHERN TERRITORY")) {
			return "NT";
		} else if (stateName.trim().equalsIgnoreCase("AUSTRALIAN CAPITAL TERRITORY")) {
			return "ACT";
		} else if (stateName.trim().equalsIgnoreCase("WESTERN AUSTRALIA")) {
			return "WA";
		}
		
		return "";
	}
}
