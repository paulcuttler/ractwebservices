package com.ract.ws.core.client;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Pattern;

import com.ract.client.ClientVO;
import com.ract.common.AddressVO;
import com.ract.common.PostalAddressVO;
import com.ract.common.ResidentialAddressVO;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.LogUtil;

/**
 * Master data representation of Client for transfer between source systems and MRM.
 * @author newtong
 *
 */
public class Client {

	public static final String DEFAULT_COUNTRY = "AUSTRALIA";
	public static final String DEFAULT_COUNTRY_CODE = "AU";
	public static final String DEFAULT_COUNTRY_CODE_ALT = "AUS";
	private static final String DATE_FORMAT_ISO_MIDNIGHT = "yyyy-MM-dd'T'00:00:00";
		
	private Integer clientNumber;
	private String status = com.ract.client.Client.STATUS_ACTIVE;
	private String clientType; // Business/Individual 
	private Boolean groupClient;
	private String title;
	private String firstName;
	private String lastName;
	private String middleName;
	private String dateOfBirth; // needs to be a string to allow updating via .NET
	private String gender;
	private String workPhone;
	private String homePhone;
	private String mobilePhone;
	private String email;
	
	private String postalProperty;
	private String postalStreetNumber;
	private String postalStreetName;
	private String postalSuburb;
	private String postalState;
	private Integer postalPostcode;
	private String postalCountry;
	private String postalDpid;
	private String postalLongitude;
	private String postalLatitude;
	private String postalGnaf;
	private String postalAusbar;
	private String postalName;
	
	private String residentialProperty;
	private String residentialStreetNumber;
	private String residentialStreetName;
	private String residentialSuburb;
	private String residentialState;
	private Integer residentialPostcode;
	private String residentialCountry;
	private String residentialDpid;
	private String residentialLongitude;
	private String residentialLatitude;
	private String residentialGnaf;
	private String residentialAusbar;
	
	private Integer masterClientNumber;
	private String phonePassword;
	private String phoneQuestion;
	private String contactPersonName;
	private String contactPersonPhone;	
	private Date lastUpdate;
	private BigDecimal ABN;
	
	private Collection<Integer> ownedGroups;
	private Collection<Integer> groupMembers;
		
	public Client() {}
	
	/**
	 * Initialise Client from ClientVO.
	 * @param clientVO
	 * @throws RemoteException 
	 */
	public Client(ClientVO clientVO) throws RemoteException {
		this.clientNumber = clientVO.getClientNumber();
		this.status = clientVO.getStatus();
		this.clientType = clientVO.getClientType();
		this.groupClient = clientVO.getGroupClient();
		this.title = (clientVO.getTitle()) != null ? clientVO.getTitle().trim() : "";
		this.firstName = clientVO.getFirstNameOnly();
		this.lastName = clientVO.getSurname();
		this.middleName = extractMiddleName(clientVO);
		
		if (clientVO.getBirthDate() != null) {			
			this.dateOfBirth = DateUtil.formatDate(clientVO.getBirthDate(), DATE_FORMAT_ISO_MIDNIGHT);
		} else if (clientVO.getBirthDateString() != null) {
			this.dateOfBirth = clientVO.getBirthDateString();
		}
		
		this.gender = clientVO.getGender();
		this.workPhone = cleansePhone(clientVO.getWorkPhone());
		this.homePhone = cleansePhone(clientVO.getHomePhone());
		this.mobilePhone = cleansePhone(clientVO.getMobilePhone());
		this.email = cleanseEmail(clientVO.getEmailAddress());
		this.masterClientNumber = clientVO.getMasterClientNumber();
		this.phonePassword = clientVO.getPhonePassword();
		this.phoneQuestion = clientVO.getPhoneQuestion();
		this.contactPersonName = clientVO.getContactPersonName();
		this.contactPersonPhone = clientVO.getContactPersonPhone();
		this.lastUpdate = clientVO.getLastUpdate();
		this.ABN = clientVO.getABN();
		
		PostalAddressVO postalAddress = clientVO.getPostalAddress();
		if (postalAddress != null) {
			this.postalStreetName = postalAddress.getStreet();
			this.postalSuburb = postalAddress.getSuburb();
			this.postalState = postalAddress.getState();
			try {
				this.postalPostcode = Integer.parseInt(postalAddress.getPostcode());
			} catch (NumberFormatException e) {
				LogUtil.warn(this.getClass(), "Could not parse postal postcode for clientNo: " + this.clientNumber + ": " + e);
			}
			this.postalCountry = postalAddress.getCountry();
			this.postalAusbar = postalAddress.getAusbar();
		}
		
		this.postalDpid = clientVO.getPostDpid();		
		this.postalProperty = clientVO.getPostProperty();		
		this.postalStreetNumber = clientVO.getPostStreetChar();		
		this.postalLatitude = clientVO.getPostLatitude(); 
		this.postalLongitude = clientVO.getPostLongitude();
		this.postalGnaf = clientVO.getPostGnaf();
		this.postalName = clientVO.getPostalName();
				
		ResidentialAddressVO residentialAddress = clientVO.getResidentialAddress();
		if (residentialAddress != null) {
			this.residentialStreetName = residentialAddress.getStreet();
			this.residentialSuburb = residentialAddress.getSuburb();
			this.residentialState = residentialAddress.getState();
			try {
				this.residentialPostcode = Integer.parseInt(residentialAddress.getPostcode());
			} catch (NumberFormatException e) {
				LogUtil.warn(this.getClass(), "Could not parse residential postcode for clientNo: " + this.clientNumber + ": " + e);			
			}
			this.residentialCountry = residentialAddress.getCountry();
		}
		
		this.residentialDpid = clientVO.getResiDpid();
		this.residentialProperty = clientVO.getResiProperty();		
		this.residentialStreetNumber = clientVO.getResiStreetChar();		
		this.residentialLatitude = clientVO.getResiLatitude(); 
		this.residentialLongitude = clientVO.getResiLongitude();
		this.residentialGnaf = clientVO.getResiGnaf();
		this.residentialAusbar = clientVO.getResiAusbar();
	}
	
	/**
	 * Extract middle name from given names - first name
	 * @param clientVO
	 * @return
	 */
	private String extractMiddleName(ClientVO clientVO) {
		String givenNames = clientVO.getGivenNames();		
		String middleName = (givenNames != null) ? givenNames.replaceFirst(clientVO.getFirstNameOnly(), "") : "";
		
		return middleName.trim();
	}
		
	/**
	 * Represent an email as blank if it does not contain an @.
	 * i.e. Not Applicable, To Follow, Refused.
	 * @param phone
	 * @return
	 */
	public static String cleanseEmail(String email) {
		if (email == null) {
			return "";
		}
		
		if (!Pattern.compile("@").matcher(email).find()) { // contains no @
			return "";
		}
				
		return email;
	}
	
	/**
	 * Represent an phone as blank if it does not contain 0-9.
	 * i.e. Not Applicable, To Follow, Refused.
	 * @param phone
	 * @return
	 */
	public static String cleansePhone(String phone) {
		if (phone == null) {
			return "";
		}
		
		if (!Pattern.compile("[0-9]").matcher(phone).find()) { // contains no numbers
			return "";
		}
				
		return phone;
	}
	
	/**
	 * @return the clientNumber
	 */
	public Integer getClientNumber() {
		return clientNumber;
	}

	/**
	 * @param clientNumber the clientNumber to set
	 */
	public void setClientNumber(Integer clientNumber) {
		this.clientNumber = clientNumber;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the clientType
	 */
	public String getClientType() {
		return clientType;
	}

	/**
	 * @param clientType the clientType to set
	 */
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the workPhone
	 */
	public String getWorkPhone() {
		return workPhone;
	}

	/**
	 * @param workPhone the workPhone to set
	 */
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	/**
	 * @return the homePhone
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * @param homePhone the homePhone to set
	 */
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the postalName
	 */
	public String getPostalProperty() {
		return postalProperty;
	}

	/**
	 * @param postalName the postalName to set
	 */
	public void setPostalProperty(String postalName) {
		this.postalProperty = postalName;
	}

	/**
	 * @return the postalStreetNumber
	 */
	public String getPostalStreetNumber() {
		return postalStreetNumber;
	}

	/**
	 * @param postalStreetNumber the postalStreetNumber to set
	 */
	public void setPostalStreetNumber(String postalStreetNumber) {
		this.postalStreetNumber = postalStreetNumber;
	}

	/**
	 * @return the postalStreetName
	 */
	public String getPostalStreetName() {
		return postalStreetName;
	}

	/**
	 * @param postalStreetName the postalStreetName to set
	 */
	public void setPostalStreetName(String postalStreetName) {
		this.postalStreetName = postalStreetName;
	}

	/**
	 * @return the postalSuburb
	 */
	public String getPostalSuburb() {
		return postalSuburb;
	}

	/**
	 * @param postalSuburb the postalSuburb to set
	 */
	public void setPostalSuburb(String postalSuburb) {
		this.postalSuburb = postalSuburb;
	}

	/**
	 * @return the postalState
	 */
	public String getPostalState() {
		return postalState;
	}

	/**
	 * @param postalState the postalState to set
	 */
	public void setPostalState(String postalState) {
		this.postalState = postalState;
	}

	/**
	 * @return the postalPostcode
	 */
	public Integer getPostalPostcode() {
		return postalPostcode;
	}

	/**
	 * @param postalPostcode the postalPostcode to set
	 */
	public void setPostalPostcode(Integer postalPostcode) {
		this.postalPostcode = postalPostcode;
	}

	/**
	 * @return the postalCountry
	 */
	public String getPostalCountry() {
		return (postalCountry == null || postalCountry.isEmpty() ? Client.DEFAULT_COUNTRY : postalCountry);
	}

	/**
	 * @param postalCountry the postalCountry to set
	 */
	public void setPostalCountry(String postalCountry) {
		this.postalCountry = postalCountry;
	}

	/**
	 * @return the postalDpid
	 */
	public String getPostalDpid() {
		return postalDpid;
	}

	/**
	 * @param postalDpid the postalDpid to set
	 */
	public void setPostalDpid(String postalDpid) {
		this.postalDpid = postalDpid;
	}

	/**
	 * @return the residentialName
	 */
	public String getResidentialProperty() {
		return residentialProperty;
	}

	/**
	 * @param residentialName the residentialName to set
	 */
	public void setResidentialProperty(String residentialName) {
		this.residentialProperty = residentialName;
	}

	/**
	 * @return the residentialStreetNumber
	 */
	public String getResidentialStreetNumber() {
		return residentialStreetNumber;
	}

	/**
	 * @param residentialStreetNumber the residentialStreetNumber to set
	 */
	public void setResidentialStreetNumber(String residentialStreetNumber) {
		this.residentialStreetNumber = residentialStreetNumber;
	}

	/**
	 * @return the residentialStreetName
	 */
	public String getResidentialStreetName() {
		return residentialStreetName;
	}

	/**
	 * @param residentialStreetName the residentialStreetName to set
	 */
	public void setResidentialStreetName(String residentialStreetName) {
		this.residentialStreetName = residentialStreetName;
	}

	/**
	 * @return the residentialSuburb
	 */
	public String getResidentialSuburb() {
		return residentialSuburb;
	}

	/**
	 * @param residentialSuburb the residentialSuburb to set
	 */
	public void setResidentialSuburb(String residentialSuburb) {
		this.residentialSuburb = residentialSuburb;
	}

	/**
	 * @return the residentialState
	 */
	public String getResidentialState() {
		return residentialState;
	}

	/**
	 * @param residentialState the residentialState to set
	 */
	public void setResidentialState(String residentialState) {
		this.residentialState = residentialState;
	}

	/**
	 * @return the residentialPostcode
	 */
	public Integer getResidentialPostcode() {
		return residentialPostcode;
	}

	/**
	 * @param residentialPostcode the residentialPostcode to set
	 */
	public void setResidentialPostcode(Integer residentialPostcode) {
		this.residentialPostcode = residentialPostcode;
	}

	/**
	 * @return the residentialCountry
	 */
	public String getResidentialCountry() {
		return (residentialCountry == null || residentialCountry.isEmpty() ? Client.DEFAULT_COUNTRY : residentialCountry);
	}

	/**
	 * @param residentialCountry the residentialCountry to set
	 */
	public void setResidentialCountry(String residentialCountry) {
		this.residentialCountry = residentialCountry;
	}

	/**
	 * @return the residentialDpid
	 */
	public String getResidentialDpid() {
		return residentialDpid;
	}

	/**
	 * @param residentialDpid the residentialDpid to set
	 */
	public void setResidentialDpid(String residentialDpid) {
		this.residentialDpid = residentialDpid;
	}

	/**
	 * @return the residentialLongitude
	 */
	public String getResidentialLongitude() {
		return residentialLongitude;
	}

	/**
	 * @param residentialLongitude the residentialLongitude to set
	 */
	public void setResidentialLongitude(String residentialLongitude) {
		this.residentialLongitude = residentialLongitude;
	}

	/**
	 * @return the residentialLatitude
	 */
	public String getResidentialLatitude() {
		return residentialLatitude;
	}

	/**
	 * @param residentialLatitude the residentialLatitude to set
	 */
	public void setResidentialLatitude(String residentialLatitude) {
		this.residentialLatitude = residentialLatitude;
	}

	/**
	 * @return the residentialGnaf
	 */
	public String getResidentialGnaf() {
		return residentialGnaf;
	}

	/**
	 * @param residentialGnaf the residentialGnaf to set
	 */
	public void setResidentialGnaf(String residentialGnaf) {
		this.residentialGnaf = residentialGnaf;
	}

	public Boolean getGroupClient() {
		return groupClient;
	}

	public void setGroupClient(Boolean groupClient) {
		this.groupClient = groupClient;
	}	

	/**
	 * @return the masterClientNumber
	 */
	public Integer getMasterClientNumber() {
		return masterClientNumber;
	}

	/**
	 * @param masterClientNumber the masterClientNumber to set
	 */
	public void setMasterClientNumber(Integer masterClientNumber) {
		this.masterClientNumber = masterClientNumber;
	}

	/**
	 * @return the phonePassword
	 */
	public String getPhonePassword() {
		return phonePassword;
	}

	/**
	 * @param phonePassword the phonePassword to set
	 */
	public void setPhonePassword(String phonePassword) {
		this.phonePassword = phonePassword;
	}

	/**
	 * @return the phoneQuestion
	 */
	public String getPhoneQuestion() {
		return phoneQuestion;
	}

	/**
	 * @param phoneQuestion the phoneQuestion to set
	 */
	public void setPhoneQuestion(String phoneQuestion) {
		this.phoneQuestion = phoneQuestion;
	}

	/**
	 * @return the contactPersonName
	 */
	public String getContactPersonName() {
		return contactPersonName;
	}

	/**
	 * @param contactPersonName the contactPersonName to set
	 */
	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	/**
	 * @return the contactPersonPhone
	 */
	public String getContactPersonPhone() {
		return contactPersonPhone;
	}

	/**
	 * @param contactPersonPhone the contactPersonPhone to set
	 */
	public void setContactPersonPhone(String contactPersonPhone) {
		this.contactPersonPhone = contactPersonPhone;
	}

	/**
	 * @return the postalAusbar
	 */
	public String getPostalAusbar() {
		return postalAusbar;
	}

	/**
	 * @param postalAusbar the postalAusbar to set
	 */
	public void setPostalAusbar(String postalAusbar) {
		this.postalAusbar = postalAusbar;
	}

	public Collection<Integer> getOwnedGroups() {
		return ownedGroups;
	}

	public void setOwnedGroups(Collection<Integer> ownedGroups) {
		this.ownedGroups = ownedGroups;
	}

	/**
	 * @return the postalLongitude
	 */
	public String getPostalLongitude() {
		return postalLongitude;
	}

	/**
	 * @param postalLongitude the postalLongitude to set
	 */
	public void setPostalLongitude(String postalLongitude) {
		this.postalLongitude = postalLongitude;
	}

	/**
	 * @return the postalLatitude
	 */
	public String getPostalLatitude() {
		return postalLatitude;
	}

	/**
	 * @param postalLatitude the postalLatitude to set
	 */
	public void setPostalLatitude(String postalLatitude) {
		this.postalLatitude = postalLatitude;
	}

	/**
	 * @return the postalGnaf
	 */
	public String getPostalGnaf() {
		return postalGnaf;
	}

	/**
	 * @param postalGnaf the postalGnaf to set
	 */
	public void setPostalGnaf(String postalGnaf) {
		this.postalGnaf = postalGnaf;
	}

	/**
	 * @return the residentialAusbar
	 */
	public String getResidentialAusbar() {
		return residentialAusbar;
	}

	/**
	 * @param residentialAusbar the residentialAusbar to set
	 */
	public void setResidentialAusbar(String residentialAusbar) {
		this.residentialAusbar = residentialAusbar;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Collection<Integer> getGroupMembers() {
		return groupMembers;
	}

	public void setGroupMembers(Collection<Integer> groupMembers) {
		this.groupMembers = groupMembers;
	}	

	public BigDecimal getABN() {
		return ABN;
	}

	public void setABN(BigDecimal aBN) {
		ABN = aBN;
	}

	@Override
	public String toString() {
		return "Client [clientNumber=" + clientNumber + ", status=" + status
				+ ", clientType=" + clientType + ", groupClient=" + groupClient
				+ ", title=" + title + ", firstName=" + firstName + ", lastName="
				+ lastName + ", middleName=" + middleName + ", dateOfBirth="
				+ dateOfBirth + ", gender=" + gender + ", workPhone=" + workPhone
				+ ", homePhone=" + homePhone + ", mobilePhone=" + mobilePhone
				+ ", email=" + email + ", postalProperty=" + postalProperty
				+ ", postalStreetNumber=" + postalStreetNumber 
				+ ", postalStreetName=" + postalStreetName
				+ ", postalSuburb=" + postalSuburb + ", postalState=" + postalState
				+ ", postalPostcode=" + postalPostcode + ", postalCountry="
				+ postalCountry + ", postalDpid=" + postalDpid + ", postalLongitude="
				+ postalLongitude + ", postalLatitude=" + postalLatitude
				+ ", postalGnaf=" + postalGnaf + ", postalAusbar=" + postalAusbar
				+ ", residentialProperty=" + residentialProperty				
				+ ", residentialStreetNumber=" + residentialStreetNumber
				+ ", residentialStreetName=" + residentialStreetName
				+ ", residentialSuburb=" + residentialSuburb + ", residentialState="
				+ residentialState + ", residentialPostcode=" + residentialPostcode
				+ ", residentialCountry=" + residentialCountry + ", residentialDpid="
				+ residentialDpid + ", residentialLongitude=" + residentialLongitude
				+ ", residentialLatitude=" + residentialLatitude + ", residentialGnaf="
				+ residentialGnaf + ", residentialAusbar=" + residentialAusbar
				+ ", masterClientNumber=" + masterClientNumber + ", phonePassword="
				+ phonePassword + ", phoneQuestion=" + phoneQuestion
				+ ", contactPersonName=" + contactPersonName + ", contactPersonPhone="
				+ contactPersonPhone + ", ownedGroups=" + ownedGroups + ", lastUpdate=" + lastUpdate + "]";
	}

	public String getPostalName() {
		return postalName;
	}

	public void setPostalName(String postalName) {
		this.postalName = postalName;
	}
}

