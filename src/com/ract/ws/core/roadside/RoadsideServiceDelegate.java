package com.ract.ws.core.roadside;

import java.util.Collection;
import java.util.Date;

import javax.jws.WebParam;

import com.ract.util.DateTime;

@javax.jws.WebService(targetNamespace = "http://roadside.core.ws.ract.com/", serviceName = "CoreRoadsideService", portName = "CoreRoadsidePort")
public class RoadsideServiceDelegate {

	com.ract.ws.core.roadside.RoadsideService roadsideService = new com.ract.ws.core.roadside.RoadsideService();
	
	public Long getRoadsideChangesCount(
			@WebParam(name = "startDate") Date start, 
			@WebParam(name = "endDate") Date end) {
		return roadsideService.getRoadsideChangesCount(start, end);
	}
	
	public Collection<Integer> getRoadsideChangesIdentifiers(
			@WebParam(name = "startDate") Date start, 
			@WebParam(name = "endDate") Date end) {
		return roadsideService.getRoadsideChangesIdentifiers(start, end);
	}
	
	public Collection<Roadside> getRoadsideChanges(
			@WebParam(name = "startDate") Date start, 
			@WebParam(name = "endDate") Date end) {
		return roadsideService.getRoadsideChanges(start, end);
	}

	public Boolean isRoadsideChangesExist(
			@WebParam(name = "startDate") Date start, 
			@WebParam(name = "endDate") Date end) {
		return roadsideService.isRoadsideChangesExist(start, end);
	}
	
	public Roadside getRoadsideById(@WebParam(name = "roadsideId") Integer roadsideId) {
		return roadsideService.getRoadsideById(roadsideId);
	}
}
