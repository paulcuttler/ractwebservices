package com.ract.ws.core.roadside;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.xml.ws.WebServiceException;

import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipVO;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

public class RoadsideService {
	
	/**
	 * Returns the number of roadside product changes between the start and end times. 
	 * This will be used by an overnight process to ensure all reported changes have made the transition to the MRM.
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Long getRoadsideChangesCount(Date start, Date end) {
		Long count = (long) 0;
		
		if (start == null) {
			throw new WebServiceException("Start date cannot be null");
		}
		if (end == null) {
			throw new WebServiceException("End date cannot be null");
		}		
		if (end.before(start)) {
			throw new WebServiceException("End date cannot be before start date");
		}
				
		try {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			count = membershipMgr.getMembershipChangesCount(new DateTime(start), new DateTime(end));
			
		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.fatal(getClass(), e);
			throw new WebServiceException(e.getMessage());
		}
				
		LogUtil.debug(this.getClass(), "getRoadsideChangesCount() found " + count + " roadside changes for range " + start.toString() + " - " + end.toString());
		
		return count;
	}
	
	/**
	 * Return the identifiers of all roadside products changed during the date range. 
	 * Used in an overnight process in conjunction with the @getRoadsideChangesCount web method. 
	 * If a missing roadside product is found, force a sync on that roadside product.
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Collection<Integer> getRoadsideChangesIdentifiers(Date start, Date end) {
	Collection<Integer> idList =  new ArrayList<Integer>();
		
		if (start == null) {
			throw new WebServiceException("Start date cannot be null");
		}
		if (end == null) {
			throw new WebServiceException("End date cannot be null");
		}		
		if (end.before(start)) {
			throw new WebServiceException("End date cannot be before start date");
		}
		
		try {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			idList = membershipMgr.getMembershipChangesIdentifiers(new DateTime(start), new DateTime(end));
			
		} catch (Exception e) {
			LogUtil.fatal(getClass(), e);
			throw new WebServiceException(e.getMessage());
		}
				
		LogUtil.debug(this.getClass(), "getRoadsideChangesIdentifiers() found " + idList.size() + " roadside changes for range " + start.toString() + " - " + end.toString());
		
		return idList;
	}
	
	/**
	 * Returns array of Roadside objects that have been created or modified between the start and end dates (inclusive). 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Collection<Roadside> getRoadsideChanges(Date start, Date end) {
		
		LogUtil.debug(this.getClass(), "getRoadsideChanges() for range " + start.toString() + " - " + end.toString());
		
		Collection<MembershipVO> roadsideVoList = new ArrayList<MembershipVO>();
		Collection<Roadside> roadsideList = new ArrayList<Roadside>();
		
		if (start == null) {
			throw new WebServiceException("Start date cannot be null");
		}
		if (end == null) {
			throw new WebServiceException("End date cannot be null");
		}		
		if (end.before(start)) {
			throw new WebServiceException("End date cannot be before start date");
		}
		
		try {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			roadsideVoList = membershipMgr.getMembershipChanges(new DateTime(start), new DateTime(end));
			
		} catch (Exception e) {
			LogUtil.fatal(getClass(), e);
			throw new WebServiceException(e.getMessage());
		}
		
		for (MembershipVO memVO : roadsideVoList) {
			try {
				roadsideList.add(new Roadside(memVO)); // translate MembershipVO to Roadside
			} catch (Exception e) {
				LogUtil.fatal(this.getClass(), e);
			}
		}
				
		LogUtil.info(this.getClass(), "getRoadsideChanges() found " + roadsideList.size() + " roadside changes for range " + start.toString() + " - " + end.toString());
		
		return roadsideList;
	}
	
	/**
	 * Returns boolean; true if changes (creates/updates) exist between the entered dates, false otherwise.
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Boolean isRoadsideChangesExist(Date start, Date end) {
		boolean exists = false;

		if (start == null) {
			throw new WebServiceException("Start date cannot be null");
		}
		if (end == null) {
			throw new WebServiceException("End date cannot be null");
		}		
		if (end.before(start)) {
			throw new WebServiceException("End date cannot be before start date");
		}
		
		try {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			exists = membershipMgr.isMembershipChangesExist(new DateTime(start), new DateTime(end));
			
		} catch (Exception e) {
			LogUtil.fatal(getClass(), e);
			throw new WebServiceException(e.getMessage());
		}
		
		LogUtil.debug(this.getClass(), "isRoadsideChangesExist() " + (exists ? "found" : "did not find") + " roadside changes for range " + start.toString() + " - " + end.toString());
		
		return exists;
	}
	
	/**
	 * Returns single Roadside object.
	 * @param roadsideId
	 * @return
	 */
	public Roadside getRoadsideById(Integer roadsideId) {
		Roadside roadside = null;
		
		if (roadsideId == null || roadsideId.equals(0)) {
			throw new WebServiceException("RoadsideId cannot be null");
		}
		
		try {
			MembershipMgr membershipMgr = MembershipEJBHelper.getMembershipMgr();
			MembershipVO memVO = membershipMgr.getMembership(roadsideId);
			if (memVO == null) {
				throw new Exception("No Roadside record found for roadsideId: " + roadsideId);
			}
			
			roadside = new Roadside(memVO);
			
		} catch (Exception e) {
			LogUtil.fatal(getClass(), e);
			throw new WebServiceException(e.getMessage());
		}
		
		return roadside;
	}

}
