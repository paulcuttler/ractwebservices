package com.ract.ws.core.roadside;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Date;

import com.ract.membership.MembershipGroupVO;
import com.ract.membership.MembershipVO;
import com.ract.payment.PaymentMethod;
import com.ract.util.LogUtil;

/**
 * Master data representation of Roadside business for transfer between source systems and MRM.
 * 
 * @author newtong
 *
 */
public class Roadside {

	private Integer clientNumber;
	private Integer roadsideId;
	private String productType;
	private Date joinDate;
	private Date expiryDate;
	private String status;
	private Integer groupId;
	private Boolean primeAddressee;
	private String paymentMethod;
	private String profileCode;
	private String rego;
	private String make;
	private String model;
	private BigDecimal value;
	private Date lastUpdate;
	
	public Roadside() {}	

	/**
	 * Initialise Roadside from MembershipVO.
	 * @param membershipVO
	 * @throws RemoteException
	 */
	public Roadside(MembershipVO membershipVO) throws RemoteException {
		this.clientNumber = membershipVO.getClientNumber();
		this.roadsideId = membershipVO.getMembershipID();
		this.productType = membershipVO.getProductCode();
		this.joinDate = membershipVO.getJoinDate();
		this.expiryDate = membershipVO.getExpiryDate();				
		this.status = membershipVO.getBaseStatus();

		MembershipGroupVO groupVO = membershipVO.getMembershipGroup();
		if (groupVO != null) {
			this.groupId = groupVO.getMembershipGroupID();
		}
		
		this.primeAddressee = membershipVO.isPrimeAddressee();		
		this.paymentMethod = (membershipVO.getDirectDebitAuthority() != null ? PaymentMethod.DIRECT_DEBIT : PaymentMethod.RECEIPTING);
		
		this.value = membershipVO.getMembershipValue(true).setScale(2, BigDecimal.ROUND_HALF_UP);
		this.profileCode = membershipVO.getMembershipProfileCode();
		this.rego = membershipVO.getRego();
		
		// Correctly update make and model - GS 6/9/19
		this.make = membershipVO.getMake();
		this.model = membershipVO.getModel();
		
		this.lastUpdate = membershipVO.getLastUpdate();
	}
	
	/**
	 * @return the clientNumber
	 */
	public Integer getClientNumber() {
		return clientNumber;
	}
	/**
	 * @param clientNumber the clientNumber to set
	 */
	public void setClientNumber(Integer clientNumber) {
		this.clientNumber = clientNumber;
	}
	/**
	 * @return the roadsideId
	 */
	public Integer getRoadsideId() {
		return roadsideId;
	}
	/**
	 * @param roadsideId the roadsideId to set
	 */
	public void setRoadsideId(Integer roadsideId) {
		this.roadsideId = roadsideId;
	}
	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}
	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	/**
	 * @return the joinDate
	 */
	public Date getJoinDate() {
		return joinDate;
	}
	/**
	 * @param joinDate the joinDate to set
	 */
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}
	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the groupId
	 */
	public Integer getGroupId() {
		return groupId;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	/**
	 * @return the primeAddressee
	 */
	public Boolean getPrimeAddressee() {
		return primeAddressee;
	}
	/**
	 * @param primeAddressee the primeAddressee to set
	 */
	public void setPrimeAddressee(Boolean primeAddressee) {
		this.primeAddressee = primeAddressee;
	}
	/**
	 * @return the paymentMethod
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}
	/**
	 * @param paymentMethod the paymentMethod to set
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getProfileCode() {
		return profileCode;
	}

	public void setProfileCode(String profileCode) {
		this.profileCode = profileCode;
	}

	public String getRego() {
		return rego;
	}

	public void setRego(String rego) {
		this.rego = rego;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal membershipValue) {
		this.value = membershipValue;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public String toString() {
		return "Roadside [clientNumber=" + clientNumber + ", roadsideId="
				+ roadsideId + ", productType=" + productType + ", joinDate="
				+ joinDate + ", expiryDate=" + expiryDate + ", status=" + status
				+ ", groupId=" + groupId + ", primeAddressee=" + primeAddressee
				+ ", paymentMethod=" + paymentMethod + ", profileCode=" + profileCode
				+ ", rego=" + rego + ", value=" + value + ", lastUpdate=" + lastUpdate
				+ ", make=" + make + ", model=" + model
				+ "]";
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

}
