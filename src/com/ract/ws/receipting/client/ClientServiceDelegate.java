package com.ract.ws.receipting.client;

import java.rmi.RemoteException;
import java.util.*;

import javax.annotation.Resource;
import javax.annotation.security.*;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import com.ract.common.ValidationException;
import com.ract.ws.receipting.ReceiptingResponse;

@Stateless
@javax.jws.WebService(targetNamespace = "http://receipting.ws.ract.com/", serviceName = "ClientService", portName = "ClientPort")
public class ClientServiceDelegate {

	@Resource
  WebServiceContext wsctx;
	
	ClientService clientService = new ClientService();
	
	public ClientResponse getClient(@WebParam(name = "clientNo") Integer clientNo) throws ValidationException, RemoteException {
		return clientService.getClient(clientNo);
	}
}
