package com.ract.ws.receipting.client;

import com.ract.ws.receipting.BaseResult;

public class Client extends BaseResult {

	private String displayName;
	private String addressLine1;
	private String addressLine2;
	
	public Client() {}
	
	public Client(String displayName, String addressLine1, String addressLine2) {
		super();
		this.displayName = displayName;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	@Override
	public String toString() {
		return "Client [displayName=" + displayName + ", addressLine1="
				+ addressLine1 + ", addressLine2=" + addressLine2 + "]";
	}
	
}
