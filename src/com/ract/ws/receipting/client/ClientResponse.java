package com.ract.ws.receipting.client;

import java.util.Collection;

import com.ract.ws.receipting.ReceiptingResponse;
import com.ract.ws.receipting.BaseResult;

public class ClientResponse extends ReceiptingResponse {
	private Collection<Client> lines;
	
	public ClientResponse() {}

	public Collection<Client> getLines() {
		return lines;
	}

	public void setLines(Collection<Client> lines) {
		this.lines = lines;
	}
}
