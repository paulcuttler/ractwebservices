package com.ract.ws.receipting.client;

import java.rmi.RemoteException;
import java.util.*;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.ValidationException;
import com.ract.ws.receipting.BaseResult;
import com.ract.ws.receipting.Message;
import com.ract.ws.receipting.ReceiptingResponse;

public class ClientService {
		
	public ClientService() {
	}
	
	public ClientResponse getClient(Integer clientNo) throws ValidationException, RemoteException {
		ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
		ClientResponse response = new ClientResponse();
		Collection<Client> resultList = new ArrayList<Client>();
		Client client = new Client();
		
		if (clientNo == null) {
			response.setStatus(ReceiptingResponse.STATUS_ERROR);
			client.addMessage(Message.CODE_CLIENT_NOT_FOUND, Message.TYPE_ERROR, "Could not locate client by client number: " + clientNo);
		}
		
		ClientVO clientVO = clientMgr.getClient(clientNo);
		if (clientVO != null) {
			client.setDisplayName(clientVO.getDisplayName());
			
			String line1a = clientVO.getResidentialAddress().getAddressLine1();
			if (line1a != null) {
				line1a = line1a.trim();
			}
			
			String line1b = clientVO.getResidentialAddress().getAddressLine2();
			if (line1b != null) {
				line1b = line1b.trim();
			}
			
			StringBuilder address1 = new StringBuilder();
			if (line1a != null && !line1a.isEmpty()) {
				address1.append(line1a);
				if (line1b != null && !line1b.isEmpty()) {
					address1.append(", "); 
				}
			}
			address1.append(line1b);
			
			client.setAddressLine1(address1.toString());
			client.setAddressLine2(clientVO.getResidentialAddress().getAddressLine3());
		} else {
			response.setStatus(ReceiptingResponse.STATUS_ERROR);
			client.addMessage(Message.CODE_CLIENT_NOT_FOUND, Message.TYPE_ERROR, "Could not locate client by client number: " + clientNo);
		}
		
		resultList.add(client);
		response.setLines(resultList);
		
		return response;
	}
}
