package com.ract.ws.receipting;

public class Message {

	public final static String TYPE_INFO = "I";
	public final static String TYPE_WARN = "W";
	public final static String TYPE_ERROR = "E";

	public final static String CODE_PAYABLE_NOT_FOUND = "1002";
	public final static String CODE_CLIENT_NOT_FOUND = "1003";
	public final static String CODE_PARTIAL_PAYMENT_NOT_ALLOWED = "1004";
	public final static String CODE_PAYMENT_ALREADY_PAID = "1007";
	public final static String CODE_RECEIPT_ERROR = "1005";
	public final static String CODE_OTHER = "1100";	
	
	
	private String messageCode;
	private String messageType;
	private String messageText;
	
	public Message() {}
	
	public Message(String messageCode, String messageType, String messageText) {
		super();
		this.messageCode = messageCode;
		this.messageType = messageType;
		this.messageText = messageText;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String message) {
		this.messageText = message;
	}

	@Override
	public String toString() {
		return "Message [messageCode=" + messageCode + ", messageType="
				+ messageType + ", messageText=" + messageText + "]";
	}
}
