package com.ract.ws.receipting;

import java.util.ArrayList;
import java.util.Collection;

public class ReceiptingResponse {

	public final static String STATUS_SUCCESS = "S";
	public final static String STATUS_ERROR = "E";

	private Collection<Message> errorMessages = new ArrayList<Message>();
	private String status = STATUS_SUCCESS;
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	public Collection<Message> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(Collection<Message> messages) {
		this.errorMessages = messages;
	}
	
	public void addErrorMessage(Message msg) {
		if (this.errorMessages == null) {
			this.errorMessages = new ArrayList<Message>();
		}
		
		errorMessages.add(msg);
	}
	
	public void addErrorMessage(String messageCode, String messageType, String messageText) {
		Message msg = new Message(messageCode, messageType, messageText);
		addErrorMessage(msg);
	}
	
}
