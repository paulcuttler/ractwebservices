package com.ract.ws.receipting.roadside;

import java.math.BigDecimal;

public class Receipt {
	private String payRefNo;
	private Integer clientNo;
	private Integer receiptNo;
	private BigDecimal amountPaid;
	private String receiptedBy;
	
	public Receipt() {}
	
	public Receipt(String payRefNo, Integer clientNo, Integer receiptNo, BigDecimal amountPaid, String receiptedBy) {
		this.payRefNo = payRefNo;
		this.clientNo = clientNo;
		this.receiptNo = receiptNo;
		this.amountPaid = amountPaid;
		this.receiptedBy = receiptedBy;
	}
	
	public String getPayRefNo() {
		return payRefNo;
	}
	public void setPayRefNo(String payRefNo) {
		this.payRefNo = payRefNo;
	}
	
	public Integer getClientNo() {
		return clientNo;
	}
	public void setClientNo(Integer clientNo) {
		this.clientNo = clientNo;
	}
	
	public Integer getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(Integer receiptNo) {
		this.receiptNo = receiptNo;
	}
	
	public BigDecimal getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}
	
	public String getReceiptedBy() {
		return receiptedBy;
	}
	public void setReceiptedBy(String receiptedBy) {
		this.receiptedBy = receiptedBy;
	}

	@Override
	public String toString() {
		return "Receipt [payRefNo=" + payRefNo + ", clientNo=" + clientNo
				+ ", receiptNo=" + receiptNo + ", amountPaid=" + amountPaid
				+ ", receiptedBy=" + receiptedBy + "]";
	}
	
}
