package com.ract.ws.receipting.roadside;

import java.util.Collection;

import com.ract.ws.receipting.ReceiptingResponse;

public class RoadsideResponse extends ReceiptingResponse {

	private Collection<Payable> lines;
	
	public RoadsideResponse() {}

	public Collection<Payable> getLines() {
		return lines;
	}

	public void setLines(Collection<Payable> lines) {
		this.lines = lines;
	}
}
