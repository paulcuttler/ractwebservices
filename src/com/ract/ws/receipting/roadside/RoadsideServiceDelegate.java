package com.ract.ws.receipting.roadside;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.jws.WebParam;

@Stateless
@javax.jws.WebService(targetNamespace = "http://receipting.ws.ract.com/", serviceName = "RoadsideService", portName = "RoadsidePort")
public class RoadsideServiceDelegate {

	RoadsideService roadsideService = new RoadsideService();
	
	public RoadsideResponse getLineItems(
			@WebParam(name = "clientNo") Integer clientNo, 
			@WebParam(name = "payRefNo") String payRefNo) {		
		return roadsideService.getLineItems(clientNo, payRefNo);
	}
	
	public RoadsideResponse validateLines(@WebParam(name = "receiptList") Collection<Receipt> receiptList) {		
		return roadsideService.validateLines(receiptList);
	}
	
	public RoadsideResponse finaliseLines(@WebParam(name = "receiptList") Collection<Receipt> receiptList) {		
		return roadsideService.finaliseLines(receiptList);
	}
}
