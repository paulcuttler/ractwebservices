package com.ract.ws.receipting.roadside.da;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.jws.WebParam;

@Stateless
@javax.jws.WebService(targetNamespace = "urn:RoadsideService:RoadsidePort", serviceName = "RoadsideDAService", portName = "RoadsideDAPort")
public class RoadsideServiceDelegateDA {

	RoadsideServiceDA roadsideService = new RoadsideServiceDA();
	
	public RoadsideResponseDA getLineItems(
			@WebParam(name = "clientNo") Integer clientNo, 
			@WebParam(name = "payRefNo") String payRefNo) {		
		return roadsideService.getLineItems(clientNo, payRefNo);
	}
	
	public RoadsideResponseDA validateLines(@WebParam(name = "receiptList") Collection<ReceiptDA> receiptList) {		
		return roadsideService.validateLines(receiptList);
	}
	
	public RoadsideResponseDA finaliseLines(@WebParam(name = "receiptList") Collection<ReceiptDA> receiptList) {		
		return roadsideService.finaliseLines(receiptList);
	}
}
