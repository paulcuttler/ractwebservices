package com.ract.ws.receipting.roadside.da;

import java.util.Collection;

import com.ract.ws.receipting.ReceiptingResponse;

public class RoadsideResponseDA extends ReceiptingResponse {

	private Collection<PayableDA> lines;
	
	public RoadsideResponseDA() {}

	public Collection<PayableDA> getLines() {
		return lines;
	}

	public void setLines(Collection<PayableDA> lines) {
		this.lines = lines;
	}
}
