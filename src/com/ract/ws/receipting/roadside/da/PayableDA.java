package com.ract.ws.receipting.roadside.da;

import java.math.BigDecimal;
import java.util.Collection;

import com.ract.ws.receipting.BaseResult;
import com.ract.ws.receipting.Message;

public class PayableDA extends BaseResult {

	static public final String BPAY_IVR_PREFIX = "900";
	
	private String description;
	private BigDecimal amountOutstanding;
	private String payRefNo;
	private String productCode;
	private Integer clientNo;
		
	public PayableDA() {}
	
	public PayableDA(String description, BigDecimal amountOutstanding, String payRefNo, String productCode, Integer clientNo) {
		super();
		this.description = description;
		this.amountOutstanding = amountOutstanding;
		this.payRefNo = payRefNo;
		this.productCode = productCode;
		this.clientNo = clientNo;
	}
	
	public boolean isBpayIvr() {
		return payRefNo.toString().startsWith(BPAY_IVR_PREFIX);
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getAmountOutstanding() {
		return (amountOutstanding == null) ? null : amountOutstanding.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	public void setAmountOutstanding(BigDecimal amountOutstanding) {
		this.amountOutstanding = amountOutstanding;
	}
	public String getPayRefNo() {
		return payRefNo;
	}
	public void setPayRefNo(String payRefNo) {
		this.payRefNo = payRefNo;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String prodCode) {
		this.productCode = prodCode;
	}

	public Integer getClientNo() {
		return clientNo;
	}

	public void setClientNo(Integer clientNo) {
		this.clientNo = clientNo;
	}

	@Override
	public String toString() {
		return "Payable [description=" + description + ", amountOutstanding="
				+ amountOutstanding + ", payRefNo=" + payRefNo + ", productCode="
				+ productCode + ", clientNo=" + clientNo + "]";
	}
}
